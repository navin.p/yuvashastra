//
//  Constant.swift
//  Petnod
//
//  Created by admin on 2/7/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import CoreTelephony
import MessageUI
import CoreData
import MapKit
import SystemConfiguration
import AVKit


//MARK:
var nsud = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//var mainStoryboard : UIStoryboard = UIStoryboard()
var storyboard_name = String()
var btnOverLay = UIButton()
var dict_Login_Data = NSMutableDictionary()
//MARK: Aler Messages
var strInternetImage = UIImage(named:"cloud-computing")!
var strDataNotFoundImage = UIImage(named:"cloud-computing")!
var strSomthingWantWrong = UIImage(named:"404")!

var primaryColor = "f7b609"
var primaryBlueColor = "242489"
var primaryGreenColor = "6EB92A"


var app_Version : String = "1.0.0"
var app_VersionDate : String = "22/04/2019"
var app_VersionSupport : String = "Requires iOS 11.0 or later.Compatible with iPhone."


var application_ID : String = "https://itunes.apple.com/in/app/greengene/id1429856765?mt=8"
var Platform = "iPhone"
var Website = "http://www.infocrats.com/"
var app_VersionInfo : String = "Version - \(app_Version) \nVersion Date - \(app_VersionDate) \n\(app_VersionSupport)"


var AppName = "YuvaShastra"
var alertMessage = "Alert!"
var alertInfo = "Information!"
var alertInternet = "No Internet Connection, try later!"
var alertDataNotFound = "Sorry , Data is not Available!"
var alertSomeError = "Somthing went wrong please try again!"
var alertCalling = "Your device doesn't support this feature."
var alertLogout = "Are you sure want to logout?"
var Alert_SelectionMessage = "Please Choose a source type"
var Alert_Gallery = " Choose from Gallery"
var Alert_Camera = "Take Photo"
var Alert_Preview = "Preview"
var Alert_pdf = "PDF File"

var alertMailComposer = "Please configure your email-id in device to send email"
var Alert_OTP = "OTP required!"
var alertPassword = "Password required!"
var alertCPassword = "Confirm Password required!"
var Alert_CPasswordmatch = "Password did't match!"
var Alert_OldPassword = "Old Password required!"
//Login
var login_MobileNumber = "Mobile Number Required!"
var login_MobileNumberLimit = "Mobile Number Invalid!"
var login_Password = "Password Invalid!"
var alertEmail = "Email ID required!"
var alertEmailValid = "Email ID invalid!"

var alertAreaexpertise = "Please select area of expertise!"

var Alert_PasswordLimit = "Password should be minimum 6 digit's required!"
var Alert_FullName = "Full name required!"
var Alert_Password = "Password required!"
var Alert_CPasswordCheck = "Password did't match!"
var Alert_CPassword = "Confirm password required!"
var Alert_QualificationLimit = "Maximum limit reached!"


////MARK:
//MARK:  WEB SERVICES URL

var BaseURL :String = "http://yuvashastraapi.citizencop.org/api/mobile/"
 var BaseURLImageDownload :String = "http://yuvashastraapi.citizencop.org/ProfileImage/"
var BaseURLDocumentDownload :String = "http://yuvashastraapi.citizencop.org/document/"

var BaseURLImageUpload:String = "http://yuvashastraapi.citizencop.org/api/file/UploadAsyncPI"
var BaseURLDocumentImageUpload:String = "http://yuvashastraapi.citizencop.org/api/file/UploadAsync"

var API_teambehind :String =  "http://yuvashastraapi.citizencop.org/content/teambehind"
var API_disclaimer :String =  "http://yuvashastraapi.citizencop.org/content/disclaimer"
var API_developerinfo :String =  "http://yuvashastraapi.citizencop.org/content/developerinfo"
var API_aboutapp :String =  "http://yuvashastraapi.citizencop.org/content/aboutapp"


var API_GetQualificationDropdown :String =  BaseURL + "GetQualificationDropdown"
var API_GetCategoryDropdown :String =  BaseURL + "GetCategoryDropdown"
var API_GetUserByCategoryId :String =  BaseURL + "GetUserByCategoryId?"
var API_GetLogin :String =  BaseURL + "GetLogin?"
var API_Register :String =  BaseURL + "AddUpdateRegistration?"
var API_ForgetPassword :String =  BaseURL + "ForgetPassword?"
var API_UserVerificationByOTP :String =  BaseURL + "UserVerificationByOTP?"
var API_ResendOTP :String =  BaseURL + "ResendOTP?"
var API_GetMember :String =  BaseURL + "UserByCategoryRole?"
var API_GetChatList :String =  BaseURL + "UserChatBoardReport?"
var API_AddChat :String =  BaseURL + "AddChat?"
var API_ChangePassword :String =  BaseURL + "ChangePassword?"
var API_GetNews :String =  BaseURL + "GetNews?"
var API_GetArticle :String =  BaseURL + "GetArticle?"
var API_AddArticle :String =  BaseURL + "AddArticle?"

var API_GetInspirationalStories :String =  BaseURL + "GetInspirationalStories?"
var API_AddInspirationalStories :String =  BaseURL + "AddInspirationalStories?"
var API_GetAnswer :String =  BaseURL + "GetQuestionAnswerByUserId?"
var API_GetQuestion :String =  BaseURL + "GetQuestion?"
var API_AddQuestionLikesComments :String =  BaseURL + "AddQuestionLikesComments?"
var API_GetQuestionLikesComments :String =  BaseURL + "GetQuestionLikesComments?"

var API_AddAnswerLikesComments :String =  BaseURL + "AddAnswerLikesComments?"
var API_GetAnswerLikesComments :String =  BaseURL + "GetAnswerLikesComments?"
var API_AddAnswer :String =  BaseURL + "AddAnswer?"
var API_AddQuestion :String =  BaseURL + "AddQuestion?"
var API_GetSubCategoryDropdown :String =  BaseURL + "GetSubCategoryDropdown?"
var API_GetNestedSubCategoryDropdown :String =  BaseURL + "GetNestedSubCategoryDropdown?"
var API_AddUpdateDeviceRegistration :String =  BaseURL + "AddUpdateDeviceRegistration?"



//MARK:
//MARK: ScreenSize&DeviceType
struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
    static let SCREEN_MAX_LENGTH  = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}

struct DeviceType{
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 480.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
     static let IS_IPHONE_XR_XS_MAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH >= 1024.0
}

// MARK: - --------------Show Error / Network
// MARK: -
var btnRetry = UIButton()
var imgErrorView = UIImageView()
var lblError = UILabel()
var viewError = UIView()

func addErrorImageOnView(strMessage : String , img : UIImage , controller : UIViewController , viewMaxY : Int)  {
    
    viewError.frame =  CGRect(x: 0, y: viewMaxY + 1 , width: Int(controller.view.frame.width), height: Int(controller.view.frame.height) - viewMaxY + 1)
    
    viewError.isUserInteractionEnabled = true
    imgErrorView.frame = CGRect(x: viewError.frame.width/2 - 90, y: viewError.frame.height/2 - 120 , width: 180, height: 180)
    imgErrorView.image = img
    // imgErrorView.backgroundColor = UIColor.purple
    imgErrorView.contentMode = .scaleAspectFit
    lblError.frame = CGRect(x: 20, y:  imgErrorView.frame.maxY + 5 , width: controller.view.frame.width - 40 , height: 40)
    lblError.numberOfLines = 4
    lblError.text = strMessage
    lblError.textAlignment = .center
    lblError.textColor = UIColor.gray
    
    btnRetry = UIButton(type: .system)
    btnRetry.frame = CGRect(x: controller.view.frame.width/2 - 75, y:  lblError.frame.maxY + 20  , width: 150 , height: 30)
    btnRetry.setTitle("Retry", for: .normal)
    btnRetry.tag = 0
    btnRetry.setTitleColor(UIColor.gray, for: .normal)
    btnRetry.tintColor = UIColor.gray
    btnRetry.titleLabel?.textAlignment = .center
    btnRetry.layer.cornerRadius = 4.0
    btnRetry.layer.borderWidth = 1.0
    btnRetry.layer.borderColor = UIColor.lightGray.cgColor
    viewError.backgroundColor = UIColor.white
    viewError.addSubview(imgErrorView)
    viewError.addSubview(lblError)
    viewError.addSubview(btnRetry)
    controller.view.addSubview(viewError)
}
func removeErrorView(){
    btnRetry.removeFromSuperview()
    lblError.removeFromSuperview()
    imgErrorView.removeFromSuperview()
    viewError.removeFromSuperview()
}
//MARK:
//MARK: OTHER FUNCTION

func buttonRounded(sender : UIButton){
    
    sender.layer.cornerRadius = 22
    sender.layer.borderWidth = 1
    sender.layer.borderColor = UIColor.clear.cgColor
    sender.layer.masksToBounds = false
    sender.layer.shadowColor = UIColor.darkGray.cgColor
    sender.layer.shadowOffset = CGSize(width: 1, height: 1);
    sender.layer.shadowOpacity = 0.5
    sender.backgroundColor = hexStringToUIColor(hex: primaryColor)
    
}

func jsontoString(fromobject:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: fromobject, options: [JSONSerialization.WritingOptions.prettyPrinted]) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}
func rateForApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
    guard let url = URL(string : appId) else {
        completion(false)
        return
    }
    guard #available(iOS 10, *) else {
        completion(UIApplication.shared.openURL(url))
        return
    }
    UIApplication.shared.open(url, options: [:], completionHandler: completion)
}

//MARK:
//MARK: DATE
func getTodayString() -> String{
    let date = Date()
    let calender = Calendar.current
    let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
    let year = components.year
    let month = components.month
    let day = components.day
    let hour = components.hour
    let minute = components.minute
    let second = components.second
    let today_string = String(year!) + "_" + String(month!) + "_" + String(day!) + "_" + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
    return today_string
}

func getUniqueString()-> String{
    var strName = "\(Date()).jpg".replacingOccurrences(of: "-", with: "")
    strName = strName.replacingOccurrences(of: " ", with: "")
    strName = strName.replacingOccurrences(of: "+", with: "")
    return  strName.replacingOccurrences(of: ":", with: "")
}



//MARK:
//MARK: txtFiledValidation
func txtFiledValidation(textField : UITextField , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let decimelnumberOnly = NSCharacterSet.init(charactersIn: "0123456789.")
    let strValidStr_Digit = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
    let characterOnly = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdeghijklmnopqrstuvwxyz")

    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValidnumber = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strcharacterOnly = characterOnly.isSuperset(of: stringFromTextField as CharacterSet)

    let strValidDecimal = decimelnumberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strValidDigitCharacter = strValidStr_Digit.isSuperset(of: stringFromTextField as CharacterSet)

    if returnOnly == "NUMBER" {
        if strValidnumber == false{
            return false
        }
    }
    if returnOnly == "CHAR_DIGIT" {
        if strValidDigitCharacter == false{
            return false
        }
    }
    
    if returnOnly == "DECIMEL" {
        if strValidDecimal == false{
            return false
        }
    }
    
    if returnOnly == "CHAR" {
        if strcharacterOnly == false{
            return false
        }
    }
    
  
    
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        if(returnOnly == "DECIMEL"){
            if(string == "."){
                if(textField.text!.contains(".")){
                    return false;
                }else{
                    return true;
                }
            }
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    if (string == ".") {
        return false;
    }
    return true;
}

func txtViewValidation(textField : UITextView , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValid = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    if returnOnly == "NUMBER" {
        if strValid == false{
            return false
        }
    }
    if returnOnly == "CHAR" {
        if strValid == true{
            return false
        }
    }
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    return true;
}


func validateEmail(email: String) -> Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
}

func panCardValidation(textField1 : ACFloatingTextfield , textField2 : UITextField, string: String , type : String) -> Bool{
    let textField = type == "1" ? ACFloatingTextfield(): UITextField()
     textField.text = type == "1" ? textField1.text: textField2.text
    
        if(textField.text?.count == 0)
        {
            if(string==" ")
            {
                return false
            }
            if(string=="")
            {
                return true
            }
        }
        let cs = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").inverted
        
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        if (string == filtered)
        {
            if(string==" ")
            {
                return false
            }
            if(string=="")
            {
                return true
            }
            if(textField.text!.count == 10)
            {
                return false
            }
            
            if((textField.text!.count >= 0) && (textField.text!.count <= 4))
            {
                return string.rangeOfCharacter(from: CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ")) != nil
            }
            if((textField.text!.count > 4) && (textField.text!.count <= 8))
            {
                return string.rangeOfCharacter(from: CharacterSet(charactersIn: "0123456789")) != nil
            }
            if((textField.text!.count > 7) && (textField.text!.count <= 9))
            {
                return string.rangeOfCharacter(from: CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ")) != nil
            }
            return true
        }
        return false
    }



func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func removeNullFromDict (dict : NSMutableDictionary) -> NSMutableDictionary
{
    let dic = dict;
    for (key, value) in dict {
        let val : NSObject = value as! NSObject;
        if(val.isEqual(NSNull()))
        {
            dic.setValue("", forKey: (key as? String)!)
        } else if(val.isEqual("<null>"))
        {
            dic.setValue("", forKey: (key as? String)!)
        }
        else
        {
            dic.setValue(value, forKey: key as! String)
        }
    }
    return dic;
}



//MARK:
//MARK: Local Directory

func getImagefromDirectory(strname : String) -> UIImage{
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    if let dirPath          = paths.first
    {
        if strname == ""{
            return UIImage()
        }else{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(strname)
            if(imageURL.path.count != 0){
                return  UIImage(contentsOfFile: imageURL.path)!
            }
             return UIImage()
        }
    }
    return UIImage()
}


func removeImageFromDirectory(itemName:String) {
    let fileManager = FileManager.default
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
    let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    guard let dirPath = paths.first else {
        return
    }
    let filePath = "\(dirPath)/\(itemName)"
    print("Delete file Name : \(itemName)")
    do {
        try fileManager.removeItem(atPath: filePath)
    } catch let error as NSError {
        print(error.debugDescription)
    }
}

//MARK:
//MARK: Related to date formet

func dateTimeConvertor(str: String , formet : String) -> String {
    if str != "" && str != "<null>" {
        var fullNameArr = str.components(separatedBy: ".")
        let strFirst = fullNameArr[0] // First
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        if(formet != "Sort"){
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        else{
            dateFormatter.dateFormat = "MM/dd/yyyy"
            if  (dateFormatter.date(from: strFirst) == nil) {
                dateFormatter.dateFormat = "dd/MM/yyyy"
            }
        }
        let date = dateFormatter.date(from: strFirst)!
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dateFormatter.locale = tempLocale
        let dateString1 = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.locale = tempLocale
       // let dateString2 = dateFormatter.string(from: date)
       // let finlString = "Date : \(dateString1)  Time :  \(dateString2)"
        let finlString = "\(dateString1)"
        if(finlString == ""){
            return  " "
        }
        return finlString
    }else{
         return "Not Available"
    }
}
//yyyy-MM-dd'T'HH:mm:ss
func dateStringToFormatedDateString(dateToConvert: String, dateFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    let myDate = dateFormatter.date(from: dateToConvert)!
    dateFormatter.dateFormat = dateFormat
    let dateString = dateFormatter.string(from: myDate)
    return dateString
}
//MARK:
//MARK: Animation

func ShakeAnimation(textfiled: UITextField) {
    let shake = CAKeyframeAnimation(keyPath: "transform.translation.x")
    shake.duration = 0.1
    shake.repeatCount = 3
    shake.autoreverses = true
    shake.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
    textfiled.layer.add(shake, forKey: "shake")
}


//MARK:
//MARK:  Internet validation

func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}


func showAlertWithoutAnyAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}
func showToastForSomeTime(title : String , message : String , time : Int, viewcontrol : UIViewController){
    let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
    viewcontrol.present(alert, animated: true, completion: nil)
    // change to desired number of seconds (in this case 5 seconds)
    let when = DispatchTime.now() + 2
    DispatchQueue.main.asyncAfter(deadline: when){
        // your code with delay
        alert.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:
//MARK:  Calling & Message & OpenMap & Attribute string

func setAttributeText(lblText : UILabel) {
    let lblPocCell = NSAttributedString(string: lblText.text!,attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
    lblText.attributedText = lblPocCell
}

func checkNullValue(str : String) -> String! {
    var strValue = String()
    if str == "nil" || str == "<null>" {
        strValue = ""
    }else{
        strValue = "\(str)"
    }
    return strValue
    
}



func callingFunction(number : NSString) -> Bool{
    let str = number.replacingOccurrences(of: " ", with:"")
    
    if let url = URL(string: "tel://\(str)"), UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        return true
    }
    else {
        return false
    }
}

//MARK:
//MARK:  Get Data from Local
func saveDataInLocalDictionary(strEntity: String , strKey : String , data : NSMutableDictionary)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}



func saveDataInLocalArray(strEntity: String , strKey : String , data : NSMutableArray)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}


func getDataFromLocalUsingPredicate(strEntity: String ,pedicate : NSPredicate)-> NSArray {
    
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    fetchRequest.predicate = pedicate
    do {
        return (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}

func getDataFromLocal(strEntity: String , strkey : String )-> NSArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    
    do {
        return (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}

func getDataFromLocalUsingAntity(entityName : String) -> NSMutableArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
    do {
        //go get the results
        return  (try AppDelegate.getContext().fetch(fetchRequest) as NSArray).mutableCopy() as! NSMutableArray
        
    } catch {
        print("Error with request: \(error)")
    }
    return NSMutableArray()
}



func deleteAllRecords(strEntity: String ) {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let context = delegate.persistentContainer.viewContext
    
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\(strEntity)")
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
    
    do {
        try context.execute(deleteRequest)
        try context.save()
        
    } catch {
        print ("There was an error")
    }
}


func openMap(strTitle : String , strlat : String , strLong : String) {
    if !(strTitle == "<null>" || strTitle == "" || strTitle == "Not Available"){
        let latitude: CLLocationDegrees = Double(strlat)!
        let longitude: CLLocationDegrees = Double(strLong)!
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = strTitle
    
        mapItem.openInMaps(launchOptions: options)
    }
 
    
}



extension UITextField {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension String {
    func heightForString(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        
        return label.frame.height + 50
    }
    func widthForString(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        
        return label.frame.width + 50
    }
}
