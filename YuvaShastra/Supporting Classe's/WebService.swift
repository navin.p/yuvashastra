//
//  WebService.swift
//  AlamoFire_WebService
//
//  Created by admin on 22/12/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import Alamofire
import MessageUI
//import SWXMLHash
//import StringExtensionHTML
//import AEXML

class WebService: NSObject , NSURLConnectionDelegate,XMLParserDelegate {
    
    
    
    
    class func callAPIBYGET(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.result.value
                {
                    let dictdata = NSMutableDictionary()
                    let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                        "Result" == (k as AnyObject)as! String


                    })
                    if status{
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"success")
                    }
                    else {
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
                break
            case .failure(_):
                print(response.result.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }
    
    class func callAPIBYPOST_string(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseString { (response:DataResponse<String>) in
            switch(response.result)   {
            case .success(_):
                if let data = response.result.value
                {
                    // print(data)
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,"success")
                }
                break
            case .failure(_):
                print(response.result.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
        
        
    }
    
    class func callAPIBYPOST(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        
        request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.result.value
                {
                    // print(data)
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,"success")
                }
                break
            case .failure(_):
                print(response.result.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }
    // MARK: - Add headers when using a URLRequest
    class func postRequestWithHeaders(dictJson:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        if let url = URL(string: url) {
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.post.rawValue
            
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: dictJson, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            Alamofire.request(request)
                .responseJSON {
                    response in
                    debugPrint(response)
                    
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value
                        {
                            let dictdata = NSMutableDictionary()
                            dictdata.setValue(data, forKey: "data")
                            OnResultBlock((dictdata) ,"success")
                        }
                        break
                    case .failure(_):
                        print(response.result.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,"failure")
                        break
                    }
                    
            }
        }
        
    }
    // service for multiple
    class func callAPIWithMultiImage(parameter:NSDictionary,url:String,imageArray:NSMutableArray,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            
            if(imageArray.count>0)
            {
                for (_, item ) in imageArray.enumerated()
                {
                    let img = ((item as AnyObject) as! NSDictionary).value(forKey: "image") as! UIImage
                    // let img = (item as AnyObject) as! UIImage
                    
                    let imgName = ((item as AnyObject) as! NSDictionary).value(forKey: "imageName") as! String
                    
                    let fileName = ((item as AnyObject) as! NSDictionary).value(forKey: "fileName") as! String
                    
                    
                    if   let imageData = img.jpegData(compressionQuality:0.5) {
                        multiPartFormData.append(imageData, withName: "\(fileName)", fileName: imgName, mimeType: "image/png")
                    }
                }
                
            }
            
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
        }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
            switch (encodingResult){
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.responseJSON { response in
                    print(response.request ?? 0)  // original URL request
                    print(response.response ?? 0) // URL response
                    print(response.data ?? 0)     // server data
                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value
                    {
                        OnResultBlock(JSON as! NSDictionary,"success")
                    }else{
                        let dic = NSMutableDictionary.init()
                        dic.setValue("Connection Time Out ", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                let dic = NSMutableDictionary.init()
                dic.setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
            }
        }
    }
    class func callAPIWithImage(parameter:NSDictionary,url:String,image:UIImage,fileName:String,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        
        upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            
            let imageData = image.jpegData(compressionQuality:0.5)
            multiPartFormData.append(imageData!, withName: withName, fileName: fileName, mimeType: "image/png")
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
        }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
            switch (encodingResult){
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.responseJSON { response in
                    print(response.request as Any)  // original URL request
                    print(response.response as Any) // URL response
                    print(response.data as Any)     // server data
                                                                print(response.result)   // result of response serialization
               //    let statusCode = response.response?.statusCode
                    if ("\(response.result)" == "SUCCESS")
                    {
                        let dic = NSMutableDictionary.init()
                        dic.setValue("\(response.result)", forKey: "message")
                        OnResultBlock(dic,"Suceess")
                    }else{
                        let dic = NSMutableDictionary.init()
                        dic.setValue("FAILURE", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                let dic = NSMutableDictionary.init()
                dic.setValue("Connection Time Out", forKey: "message")
                OnResultBlock(dic,"failure")
            }
        }
    }
    
    class func callAPIWithpdf(parameter:NSDictionary,url:String,pdf:Data,fileName:String,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        
        upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            
            multiPartFormData.append(pdf, withName: withName, fileName: fileName, mimeType: "image/png")
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
        }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
            switch (encodingResult){
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.responseJSON { response in
//                    print(response.request)  // original URL request
//                    print(response.response) // URL response
//                    print(response.data)     // server data
//                    print(response.result)   // result of response serialization
                    //    let statusCode = response.response?.statusCode
                    if ("\(response.result)" == "SUCCESS")
                    {
                        let dic = NSMutableDictionary.init()
                        dic.setValue("\(response.result)", forKey: "message")
                        OnResultBlock(dic,"Suceess")
                    }else{
                        let dic = NSMutableDictionary.init()
                        dic.setValue("FAILURE", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                let dic = NSMutableDictionary.init()
                dic.setValue("Connection Time Out", forKey: "message")
                OnResultBlock(dic,"failure")
            }
        }
    }
    
      // MARK: - -
    // MARK: - ------------------------Convert String To Dictionary Function------------------------
   
    class func convertJsonStringToDictionary(text: String) -> [String: Any]? {
        if let data = text.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    
}



