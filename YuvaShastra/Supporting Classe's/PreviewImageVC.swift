//
//  PreviewImageVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/14/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class PreviewImageVC: UIViewController,UIScrollViewDelegate {

    var img = UIImage()
    @IBOutlet weak var imageViewLarge: UIImageView!
    
    @IBOutlet weak var scrollViewImage: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "toolbar_logo")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = UIColor.white

        imageViewLarge.image = img
        imageViewLarge.contentMode = .scaleAspectFit
        scrollViewImage.contentSize = imageViewLarge.bounds.size
        scrollViewImage.minimumZoomScale = 1.0
        scrollViewImage.maximumZoomScale = 5.0
        scrollViewImage.zoomScale = 0.0
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageViewLarge
    }
    
    
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
