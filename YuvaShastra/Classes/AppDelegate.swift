//
//  AppDelegate.swift
//  YuvaShastra
//
//  Created by Rakesh Jain on 11/03/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseInstanceID
import UserNotifications
import FirebaseMessaging


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FTIndicator.setIndicatorStyle(UIBlurEffect.Style.dark)
        
        //1 FireBase & Register Push Notification
        FirebaseApp.configure()
        self.registerForRemoteNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
     
        // 2 CoreData
        deleteAllRecords(strEntity:"AreaofExpertise")
        // 3 Push Specific Controller
        gotoViewController()
        
        // 4 UITabBarItem Selection Color
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: hexStringToUIColor(hex: primaryBlueColor)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray], for: .normal)
        
        UITabBar.appearance().tintColor = hexStringToUIColor(hex: primaryBlueColor)
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "YuvaShastra")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    class func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    func gotoViewController()  {
        if (nsud.value(forKey: "Yuva_LoginRemberStatus") != nil) {
            if (nsud.value(forKey: "Yuva_LoginRemberStatus")as! String == "true") {
                let obj_ISVC : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                navigationController.setNavigationBarHidden(true, animated: true)
                rootViewController(nav: navigationController)
                
            }else{
                let obj_ISVC : MentorDashboardVC = mainStoryboard.instantiateViewController(withIdentifier: "MentorDashboardVC") as! MentorDashboardVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                navigationController.setNavigationBarHidden(false, animated: true)
                rootViewController(nav: navigationController)
            }
        }else{
            let obj_ISVC : WelcomeScreenVC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenVC") as! WelcomeScreenVC
            let navigationController = UINavigationController(rootViewController: obj_ISVC)
            navigationController.setNavigationBarHidden(false, animated: true)
            rootViewController(nav: navigationController)
        }
        
    }
    func rootViewController(nav : UINavigationController)  {
        nav.navigationBar.barTintColor =  hexStringToUIColor(hex: primaryColor)
        nav.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 22)]
        nav.navigationBar.tintColor = UIColor.white
        nav.navigationBar.largeTitleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 30)]
        nav.navigationItem.largeTitleDisplayMode = .always
        
        self.window!.rootViewController = nav
        self.window!.makeKeyAndVisible()
        
        
    }
    
    //MARK: RegisterPushNotification
    
    func registerForRemoteNotifications()  {
        if #available(iOS 10, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                if granted == true
                {
                    print("Notification Allow")
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    Messaging.messaging().delegate = self
                }
                else
                {
                    print("Notification Don't Allow")
                }
            }
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            UIApplication.shared.applicationIconBadgeNumber = 1
        }
    }
}

// MARK: --------UNNotification/Messaging Delegate Method----------

extension AppDelegate : UNUserNotificationCenterDelegate , MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let state: UIApplication.State = UIApplication.shared.applicationState
        if state == .active {
            //            let dict = (userInfo as AnyObject)as! NSDictionary
            //            let aps = (dict.value(forKey: "aps")as! NSDictionary)
            //            let alert = (aps.value(forKey: "alert")as! NSDictionary)
            //            let strTitle = (alert.value(forKey: "title"))
            //            let strbody = (alert.value(forKey: "body"))
            FTIndicator.showNotification(withTitle: "PushNotification", message: "Yuva Notification Body ")
        }
        else  {
            hendelNotidata(userInfo: userInfo as NSDictionary)
        }
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let dictApsData : NSDictionary = response.notification.request.content.userInfo as NSDictionary
        hendelNotidata(userInfo: dictApsData)
    }
    
    func hendelNotidata(userInfo :NSDictionary){
        if (nsud.value(forKey: "Yuva_LoginRemberStatus") != nil) {
            if (nsud.value(forKey: "Yuva_LoginRemberStatus")as! String == "true") {
                let obj_ISVC : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
                obj_ISVC.tabBar.tag = 999

                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                navigationController.setNavigationBarHidden(true, animated: true)
                rootViewController(nav: navigationController)
            }
        }
    }
 

    
    
    // MARK: ---------GET TOKEN Func----------
    // MARK: -
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            nsud.setValue(uuid, forKey: "Yuva_DEVICE_ID")
            print("DEVICE_ID-------------------- : \(uuid)")
        }
        if let token = InstanceID.instanceID().token() {
            nsud.setValue("\(token)", forKey: "Yuva_FCM_Token")
            nsud.synchronize()
            print("FCM_Token----------------- : \(String(describing: token))")
            
        }
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        nsud.setValue("5437788560965635874098656heyfget87", forKey: "Yuva_FCM_Token")
        nsud.setValue("AHDkh5445", forKey: "Yuva_DEVICE_ID")
        nsud.synchronize()
    }
    
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        if let token = InstanceID.instanceID().token() {
            nsud.setValue("\(token)", forKey: "Yuva_FCM_Token")
            nsud.synchronize()
        }
        
    }
}
