//
//  QuestionListDetailVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/3/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class QuestionListDetailVC: UIViewController {
    
    @IBOutlet weak var tvlist: UITableView!
    var dictQuestionAnswerData = NSMutableDictionary()
    var aryList = NSMutableArray()
    // MARK: - ----------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
           self.navigationItem.title = "Answers"
           self.navigationController?.navigationBar.prefersLargeTitles = false
            let aryLike = (dictQuestionAnswerData.value(forKey: "answerMobileDc")as! NSArray).mutableCopy()as! NSMutableArray
            for item in aryLike{
                var dictLike = NSMutableDictionary()
                let aryLikefinal = NSMutableArray()
                dictLike = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                var aryTemp = NSMutableArray()
                aryTemp = (dictLike.value(forKey: "answerLikeCommentDc")as! NSArray).mutableCopy()as! NSMutableArray
                for item in aryTemp{
                    var dictLike = NSMutableDictionary()
                    dictLike = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    let strCommentedID = "\(dictLike.value(forKey: "UserId")!)"
                    let strType = "\(dictLike.value(forKey: "Type")!)"
                    if(("\(dict_Login_Data.value(forKey: "UserId")!)" == strCommentedID)  && (strType == "Like")){
                        aryLikefinal.add(dictLike)
                        break
                    }
                }
                dictLike.setValue(aryLikefinal, forKey: "answerLikeCommentDc")
                self.aryList.add(dictLike)
                self.tvlist.reloadData()
            }
      tvlist.reloadData()
    }
    
    // MARK: - ----------------API Calling
    // MARK: -
    func AnswerLikeApiCalling(sender : UIButton){
        let dict = removeNullFromDict(dict: (aryList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            
            let urlForGetData = "\(API_AddQuestionLikesComments)" + "QuestionId=\(dict.value(forKey: "QuestionId")!)" + "&UserId=\(dict_Login_Data.value(forKey: "UserId")!)" + "&Type=Like" + "&Comment="
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: urlForGetData) { (responce, status) in
                FTIndicator.dismissProgress()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let dictlike = NSMutableDictionary()
                        if(sender.currentImage != UIImage(named: "like_icon_2")){
                            dictlike.setValue("\(dict_Login_Data.value(forKey: "UserId")!)", forKey: "UserId")
                            dictlike.setValue("Like", forKey: "Type")
                        }else{
                            dictlike.setValue("", forKey: "UserId")
                            dictlike.setValue("Like", forKey: "Type")
                        }
                        let dictForIndex = (self.aryList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        let aryTem = NSMutableArray()
                        aryTem.add(dictlike)
                        dictForIndex.setValue(aryTem, forKey: "answerLikeCommentDc")
                        self.aryList.replaceObject(at: sender.tag, with: dictForIndex)
                        self.tvlist.reloadData()
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension QuestionListDetailVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  section == 0 ? 1 : aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath as IndexPath) as! QuestionCell
            cell.lblCategoryName.text = "\(dictQuestionAnswerData.value(forKey: "CategoryName")!)"
            cell.lblPostedby.text = "Posted by : \(dictQuestionAnswerData.value(forKey: "PostedBy")!)\n" + "Posted on :" + dateStringToFormatedDateString(dateToConvert: "\(dictQuestionAnswerData.value(forKey: "CreatedDate")!)", dateFormat: "dd/MM/yyy hh:mm a")
            cell.lblQuestion.text = "Q. \(dictQuestionAnswerData.value(forKey: "Question")!)"
            cell.lblPostedby.textColor = hexStringToUIColor(hex: primaryBlueColor)
            cell.lblCategoryName.textColor = hexStringToUIColor(hex: primaryBlueColor)
            cell.imgView.layer.borderWidth = 1.0
            cell.imgView.layer.masksToBounds = false
            cell.imgView.layer.cornerRadius = 40.0
            cell.imgView.clipsToBounds = true
            cell.imgView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            let urlImage = "\(BaseURLImageDownload)\(dictQuestionAnswerData.value(forKey: "Question")!)"
            cell.imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "aspirant_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                //print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            return cell
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "AnswerCell", for: indexPath as IndexPath) as! AnswerCell
            let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            cell.lblAns.text = "Ans. \(dict.value(forKey: "Answer")!)"
            cell.lblAnsBy.text = "Answered by : \(dict.value(forKey: "PostedBy")!)\n" +  dateStringToFormatedDateString(dateToConvert: "\(dict.value(forKey: "CreatedDate")!)", dateFormat: "dd/MM/yyy hh:mm a")
            cell.lblAnsBy.textColor = hexStringToUIColor(hex: primaryBlueColor)
            
            // *************Attachment*************//
            if(dict.value(forKey: "attachmentTableDc")as! NSArray).count != 0{
                let aryAttechment = (dict.value(forKey: "attachmentTableDc")as! NSArray)
                
                if( removeNullFromDict(dict: ((aryAttechment.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary)) .value(forKey: "AttachmentType")as! String == "image"){
                    let urlImage = "\(BaseURLDocumentDownload)\((aryAttechment.object(at: 0)as! NSDictionary).value(forKey: "AttachmentPath")!)"
                    cell.imgAns.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    }, usingActivityIndicatorStyle: .gray)
                }else if ( removeNullFromDict(dict: ((aryAttechment.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary)) .value(forKey: "AttachmentType")as! String == "pdf"){
                    cell.imgAns.image = UIImage(named: "pdf")
                }else{
                    cell.imgAns.image = UIImage(named: "")
                }
            }else{
                  cell.imgAns.image = UIImage(named: "")
            }
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
            cell.imgAns.isUserInteractionEnabled = true
            cell.imgAns.tag = indexPath.row
            cell.imgAns.addGestureRecognizer(tapGestureRecognizer)
            // ************XXXXX*************//
            
            // *************LIKE/UNLIKE*************//
            cell.btnLike.setImage(UIImage(named: "like_icon_1"), for: .normal)
            let aryLike = dict.value(forKey: "answerLikeCommentDc")as! NSArray
            for item in aryLike{
                let dict  =  (item as AnyObject)as! NSDictionary
                let strID = "\(dict.value(forKey: "UserId")!)"
                if(strID == "\(dict_Login_Data.value(forKey: "UserId")!)"){
                    cell.btnLike.setImage(UIImage(named: "like_icon_2"), for: .normal)
                    break
                }else{
                    cell.btnLike.setImage(UIImage(named: "like_icon_1"), for: .normal)
                }
            }
            cell.btnLike.addTarget(self, action: #selector(buttonLike), for: .touchUpInside)
            cell.btnLike.tag = indexPath.row
            // ************XXXXX*************//
            cell.btnShare.addTarget(self, action: #selector(buttonShare), for: .touchUpInside)
            cell.btnShare.tag = indexPath.row
            cell.btnComment.addTarget(self, action: #selector(buttonComment), for: .touchUpInside)
            cell.btnComment.tag = indexPath.row
            
            return cell
        }
  
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     
    }
    @objc func connected(_ sender:AnyObject){
        print("you tap image number : \(sender.view.tag)")
        let dict = removeNullFromDict(dict: (aryList.object(at: sender.view.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        if(dict.value(forKey: "attachmentTableDc")as! NSArray).count != 0{
            let aryAttechment = (dict.value(forKey: "attachmentTableDc")as! NSArray)
            if(removeNullFromDict(dict: ((aryAttechment.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary)) .value(forKey: "AttachmentType")as! String == "image"){
                let urlImage = "\(BaseURLDocumentDownload)\((aryAttechment.object(at: 0)as! NSDictionary).value(forKey: "AttachmentPath")!)"
                let imgView = UIImageView()
                imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "no-image"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
                    testController.img = imgView.image!
                    self.navigationController?.pushViewController(testController, animated: true)
                }, usingActivityIndicatorStyle: .gray)
            }else{
                  let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
                testController.strViewComeFrom = "Document"
                testController.webURL = "\(BaseURLDocumentDownload)\((aryAttechment.object(at: 0)as! NSDictionary).value(forKey: "AttachmentPath")!)"
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }else{
          
        }
        
        
  
    }
    
    @objc func buttonShare(sender: UIButton!) {
        let dict = removeNullFromDict(dict: (aryList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        let activityViewController = UIActivityViewController(activityItems: ["\(dict.value(forKey: "Answer")!)"], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func buttonLike(sender: UIButton!) {
      self.AnswerLikeApiCalling(sender: sender)
        
    }
    @objc func buttonComment(sender: UIButton!) {
        let dict = removeNullFromDict(dict: (aryList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "CommentVC")as! CommentVC
        testController.dictData = dict
        testController.strComeFromView = "Answer"
        self.navigationController?.pushViewController(testController, animated: true)
    }
}

// MARK: - ----------------Cell
// MARK: -
class QuestionCell: UITableViewCell {
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblPostedby: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
class AnswerCell: UITableViewCell {
    @IBOutlet weak var lblAns: UILabel!
    @IBOutlet weak var lblAnsBy: UILabel!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var imgAns: UIImageView!

    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var imgWidth: NSLayoutConstraint!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
