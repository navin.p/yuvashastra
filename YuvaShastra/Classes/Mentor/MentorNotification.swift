//
//  MentorNotification.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 3/28/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit
import CoreData

class MentorNotification: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    var aryList = NSMutableArray()
    var refresher = UIRefreshControl()
    var strErrorMessage = String()
    var intNumberofRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.refresher.tintColor = hexStringToUIColor(hex: primaryBlueColor)
        self.tvlist!.addSubview(refresher)
        self.tvlist.tableFooterView = UIView()
        strErrorMessage = "Coming Soon..."
        self.call_GetNotificationListData_API(tag: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
 
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
    //    self.call_GetNotificationListData_API(tag: 2)
    }
    
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_GetNotificationListData_API( tag : Int)  {
        if (isInternetAvailable()){
            strErrorMessage = "Coming Soon..."
            intNumberofRow = 1
            self.tvlist.reloadData()
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            //----1
            if tag == 1{
                loading.startLoading(text: "Loading...")
                // FTIndicator.showProgress(withMessage: "Please wait...")
            }
            //---2
            let dictData = NSMutableDictionary()
            var aryCategoryId = NSMutableArray()
            var strCategoryID = ""
            var strRole = "Student"
            
            aryCategoryId = (dict_Login_Data.value(forKey: "userCategoryMappingDc")as! NSArray).mutableCopy()as! NSMutableArray
            for item in aryCategoryId{
                strCategoryID = strCategoryID + "\((item as AnyObject).value(forKey: "CategoryId")!),"
            }
            if(strCategoryID.count > 0){
                strCategoryID = String(strCategoryID.dropLast())
            }
            if("\(dict_Login_Data.value(forKey: "Role")!)" == "Student"){
                strRole = "Mentor"
            }
            let urlForGetData = "\(API_GetMember)" + "CategoryIds=\(strCategoryID)"  + "&Role=\(strRole)"
            
            WebService.callAPIBYGET(parameter: dictData, url: urlForGetData ) { (responce, status) in
                if tag == 1{
                    loading.endLoading()
                }else{
                    self.refresher.endRefreshing()
                }
                removeErrorView()
                
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "UserChatBoardReportDc")as! NSArray)
                        self.aryList = NSMutableArray()
                        self.aryList = aryData.mutableCopy()as! NSMutableArray
                    }else{
                        self.strErrorMessage = "\(dict.value(forKey: "Success")as! String)"
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                }
                self.intNumberofRow = 1
                self.tvlist.reloadData()
            }
        }
    }



}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MentorNotification: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count == 0 ? intNumberofRow : aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(aryList.count != 0){
        let cell = tvlist.dequeueReusableCell(withIdentifier: "NotificationtCell", for: indexPath as IndexPath) as! NotificationtCell
        
        let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        cell.lblTitle.text = "\(dict.value(forKey: "Name")!)"
        if("\(dict_Login_Data.value(forKey: "Role")!)" == "Student"){
            cell.lblSubTitle.text = "Mentor"
        }else{
            cell.lblSubTitle.text = "Student"
        }
        return cell
    }else{
    let cell = tvlist.dequeueReusableCell(withIdentifier: "ChatCellError", for: indexPath as IndexPath) as! ChatCell
    cell.lblErrorTitle.text = strErrorMessage
    return cell
    }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return aryList.count != 0 ? UITableView.automaticDimension : self.tvlist.frame.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if( aryList.count == 0){
            intNumberofRow = 0
            self.tvlist.reloadData()
          //  self.call_GetNotificationListData_API(tag: 1)
        }
    }
    
}

// MARK: - ----------------ChatCell
// MARK: -
class NotificationtCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
