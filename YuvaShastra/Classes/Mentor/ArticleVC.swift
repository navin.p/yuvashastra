//
//  ArticleVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/2/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class ArticleVC: UIViewController {
    
    @IBOutlet weak var tvlist: UITableView!
    var aryList = NSMutableArray()
    var refresher = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Article"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let BarButtonItem = UIBarButtonItem(title: "ADD", style: .plain, target: self, action: #selector(More))
        self.navigationItem.rightBarButtonItem  = BarButtonItem
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.refresher.tintColor = hexStringToUIColor(hex: primaryBlueColor)
        self.tvlist!.addSubview(refresher)
        self.tvlist.reloadData()
   
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
             self.call_GetArticleListData_API(tag: 1)
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        self.call_GetArticleListData_API(tag: 2)
     
    }
    @objc func More(){
        self.view.endEditing(true)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddArtical_StoryVC")as! AddArtical_StoryVC
        testController.strComeFrom = "Add Article"
        self.navigationController?.pushViewController(testController, animated: true)
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_GetArticleListData_API( tag : Int)  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            //----1
            if tag == 1{
                loading.startLoading(text: "Loading...")
            }
            //---2
            let dictData = NSMutableDictionary()
            let urlForGetData = "\(API_GetArticle)" + "city_id=\(dict_Login_Data.value(forKey: "city_id")!)"
            WebService.callAPIBYGET(parameter: dictData, url: urlForGetData ) { (responce, status) in
                if tag == 1{
                    loading.endLoading()
                }else{
                    self.refresher.endRefreshing()
                }
                removeErrorView()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "ArticlesDc")as! NSArray)
                        self.aryList = NSMutableArray()
                        self.aryList = aryData.mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                        
                    }else{
                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "Success")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self, viewMaxY: Int((self.navigationController?.navigationBar.bounds.maxY)!))
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.call_GetArticleListData_API(tag: 1)
    }
    
    
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ArticleVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath as IndexPath) as! ArticleCell
        let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        cell.lblTitle.text = "\(dict.value(forKey: "Title")!)"
        cell.lblPostedBy.text = "Posted by : \(dict.value(forKey: "PostedBy")!)"
        cell.lblDate.text = "Posted on :" + dateStringToFormatedDateString(dateToConvert: "\(dict.value(forKey: "CreatedDate")!)", dateFormat: "dd/MM/yyy hh:mm a")
        cell.lblDate.textColor = hexStringToUIColor(hex: primaryBlueColor)
        cell.lblPostedBy.textColor = hexStringToUIColor(hex: primaryBlueColor)
        cell.lblDetail.text = "\(dict.value(forKey: "Description")!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    
    
    
}

// MARK: - ----------------ChatCell
// MARK: -
class ArticleCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPostedBy: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
