//
//  MentorHomeVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 3/28/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit
import CoreData
class MentorHomeVC: UIViewController {
    
    // MARK: -
    // MARK: - Outlets
   @IBOutlet weak var collection: UICollectionView!
    //Variable
    var ary_of_Collection = NSMutableArray()
    var ary_for_DropDown = NSMutableArray()
    var refresher:UIRefreshControl!
    var txtType = String()
    var txtTag = String()
    var strErrorMessage = String()

    var intNumberofRow = 0
    var strViewComeFrom = String()

    // MARK: -
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
            self.refresher = UIRefreshControl()
            self.refresher.tintColor = hexStringToUIColor(hex: primaryBlueColor)
            self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
            self.collection!.addSubview(refresher)
            txtTag = "0"
            txtType = "All"
            call_GetMentorListData_API( tag: 1)
            if(dataFromLocal(strEntity: "AreaofExpertise", strkey: "areaofExpertise").count != 0){
                self.ary_for_DropDown = NSMutableArray()
                self.ary_for_DropDown = (dataFromLocal(strEntity: "AreaofExpertise", strkey: "areaofExpertise")as NSArray).mutableCopy()as! NSMutableArray
            }else{
                self.call_GetCategoryDropdown_API()
            }
        let BarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(More))
        BarButtonItem.image = UIImage(named: "moreIcon")
        self.navigationItem.rightBarButtonItem  = BarButtonItem
        self.navigationItem.title = "Mentor"
       
}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(dataFromLocal(strEntity: "LoginData", strkey: "loginData").count != 0){
            print(dataFromLocal(strEntity: "LoginData", strkey: "loginData"))
            dict_Login_Data = NSMutableDictionary()
            dict_Login_Data = ((dataFromLocal(strEntity: "LoginData", strkey: "loginData") as NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            if nsud.value(forKey: "Yuva_FCM_Token") != nil && nsud.value(forKey: "Yuva_DEVICE_ID") != nil{
                call_API_AddUpdateDeviceRegistrationAPI(tag: 0)
            }
            
        }
         if(self.tabBarController?.tabBar.tag == 999){
            self.tabBarController?.tabBar.tag = 0
            let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "QuestionListVC") as! QuestionListVC
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @objc func More(){
        self.view.endEditing(true)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let CancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(CancelActionButton)
        let TodaysActionButton = UIAlertAction(title: "About App", style: .default) { _ in
            testController.strViewComeFrom = "About App"
            testController.webURL = API_aboutapp
            
            self.navigationController?.pushViewController(testController, animated: true)
            
        }
        actionSheetControllerIOS8.addAction(TodaysActionButton)
        
        let LastWeekActionButton = UIAlertAction(title: "Disclaimer", style: .default)
        { _ in
            testController.strViewComeFrom = "Disclaimer"
            testController.webURL = API_disclaimer
            
            self.navigationController?.pushViewController(testController, animated: true)
        }
        actionSheetControllerIOS8.addAction(LastWeekActionButton)
        
        let Last15daysActionButton = UIAlertAction(title: "Developer Info", style: .default)
        { _ in
            testController.strViewComeFrom = "Developer Info"
            testController.webURL = API_developerinfo
            
            self.navigationController?.pushViewController(testController, animated: true)
        }
        actionSheetControllerIOS8.addAction(Last15daysActionButton)
        
        let LastMonthActionButton = UIAlertAction(title: "Team Behind", style: .default)
        { _ in
            testController.strViewComeFrom = "Team Behind"
            testController.webURL = API_teambehind
            
            self.navigationController?.pushViewController(testController, animated: true)
            
        }
        actionSheetControllerIOS8.addAction(LastMonthActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)    }

    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        call_GetMentorListData_API( tag: 0)
    }
    
    // MARK: - --------------Local Data Base
    // MARK: -
    func dataFromLocal(strEntity: String ,strkey : String )-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: strEntity, strkey: strkey)
        
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: strkey) ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }

    // MARK: - ---------------API's Calling
    // MARK: -

    func call_GetCategoryDropdown_API( )  {
        if !(isInternetAvailable()){
        }else{
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: API_GetCategoryDropdown) { (responce, status) in
           
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "categoryDropdownDc")as! NSArray)
                        self.ary_for_DropDown = NSMutableArray()
                        self.ary_for_DropDown = aryData.mutableCopy()as! NSMutableArray
                        deleteAllRecords(strEntity:"AreaofExpertise")
                        saveDataInLocalArray(strEntity: "AreaofExpertise", strKey: "areaofExpertise", data: self.ary_for_DropDown)
                    }else{
                    }
                }else{
                }
            }
        }
    }
    func call_GetMentorListData_API( tag : Int)  {
        if !(isInternetAvailable()){
            strErrorMessage = alertInternet
            intNumberofRow = 1
            self.collection.reloadData()
            
        }else{
            let loading = DPBasicLoading(collection: collection, fontName: "HelveticaNeue")

            //----1
            if tag == 1{
                loading.startLoading(text: "Loading...")
                // FTIndicator.showProgress(withMessage: "Please wait...")
            }
            //---2
            let dictData = NSMutableDictionary()
            let urlForGetData = "\(API_GetUserByCategoryId)" + "CategoryId=\(txtTag)"
            
            WebService.callAPIBYGET(parameter: dictData, url: urlForGetData ) { (responce, status) in
                if tag == 1{
                    loading.endLoading()
                    //    FTIndicator.dismissProgress()
                }else{
                    self.refresher.endRefreshing()
                }
                removeErrorView()
                
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "UserByCategoryDc")as! NSArray)
                        self.ary_of_Collection = NSMutableArray()
                        self.ary_of_Collection = aryData.mutableCopy()as! NSMutableArray
                        self.collection.reloadData()
                        
                    }else{
                        self.strErrorMessage = "\(dict.value(forKey: "Success")as! String)"
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                }
                self.intNumberofRow = 1
                self.collection.reloadData()
            }
        }
    }
    
    func
        call_API_AddUpdateDeviceRegistrationAPI( tag : Int)  {
        if !(isInternetAvailable()){
        }else{
            let dictData = NSMutableDictionary()
            let strDeviceID = "\(nsud.value(forKey: "Yuva_DEVICE_ID")!)"
            let strToken = "\(nsud.value(forKey: "Yuva_FCM_Token")!)"
            let strcity_id = "\(dict_Login_Data.value(forKey: "city_id")!)"
            let strUserId = "\(dict_Login_Data.value(forKey: "UserId")!)"

            let urlForGetData = "\(API_AddUpdateDeviceRegistration)" + "DeviceId=\(strToken)"  + "&SecureKey=\(strDeviceID)" + "&city_id=\(strcity_id)" + "&UserId=\(strUserId)"
            WebService.callAPIBYGET(parameter: dictData, url: urlForGetData ) { (responce, status) in
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                    }else{
                    }
                }else{
                }
            }
        }
    }

}

//MARK:-
//MARK:- ---------PopUpDelegate

extension MentorHomeVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 31){
            self.ary_of_Collection = NSMutableArray()
            self.collection.reloadData()
           txtType = "\(dictData.value(forKey: "CategoryName")!)"
            self.txtTag = "\(dictData.value(forKey: "CategoryId")!)"
            call_GetMentorListData_API(tag: 1)
        }
    }
}



// MARK: - ----------------UICollectionViewDelegate
// MARK: -

extension MentorHomeVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return    self.ary_of_Collection.count != 0 ? self.ary_of_Collection.count : intNumberofRow
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(ary_of_Collection.count != 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MentorCell", for: indexPath as IndexPath) as! MentorCell
            let dict = removeNullFromDict(dict: (ary_of_Collection.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            cell.ListCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)"
            cell.ListCell_lbl_SubTitle.text = "\(dict.value(forKey: "CurruntPosition")!)\n(\(dict.value(forKey: "Category")!))"
            cell.ListCell_lbl_SubTitle.text = cell.ListCell_lbl_SubTitle.text?.replacingOccurrences(of: "()", with: "")
            let urlImage = "\(BaseURLImageDownload)\(dict.value(forKey: "ProfileImage")!)"
            cell.ListCell_Image.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "no-image"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                //print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            cell.imgHeight.constant = 0.0
            cell.imgHeight.constant = (self.collection.frame.size.width)/2
            return cell
        }else{
            
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionError", for: indexPath as IndexPath) as! MentorCell
            cell.lbl_ErrorTitle.text =  strErrorMessage
            return cell
        }
        
     
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  ((ary_of_Collection.count != 0)  ?  (CGSize(width: (self.collection.frame.size.width)  / 2, height: (self.collection.frame.size.width)  / 2 + 85.0 )) : CGSize(width: self.collection.frame.width, height: self.collection.frame.height - 55 ))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(ary_of_Collection.count != 0){
            let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "MentorProfileDetailVC") as! MentorProfileDetailVC
            let dict = removeNullFromDict(dict: (ary_of_Collection.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            nextViewController.dictData = dict
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }else{
            intNumberofRow = 0
            collection.reloadData()
            self.call_GetMentorListData_API(tag: 1)
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
         return CGSize(width: collectionView.frame.width, height: 55)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "dashboardview", for: indexPath) as! dashboardview
            headerView.lbl_Title.text = txtType
            headerView.btnDrop.addTarget(self, action: #selector(self.pressButton), for: .touchUpInside)
            return headerView
        case UICollectionView.elementKindSectionFooter:
            return UICollectionReusableView()
        default:
            return UICollectionReusableView()
        }
    }
    @objc func pressButton() {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "---Select Category---"
        vc.strTag = 31
        if ary_for_DropDown.count != 0{
            let dict = NSMutableDictionary()
            dict.setValue("All", forKey: "CategoryName")
            dict.setValue("0", forKey: "CategoryId")
            if !(ary_for_DropDown.contains(dict)){
                ary_for_DropDown.insert(dict, at: 0)
            }
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.aryTBL = ary_for_DropDown
            vc.delegate = self
            self.present(vc, animated: false, completion: {})
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "", viewcontrol: self)
        }
        
    }
}

class dashboardview: UICollectionReusableView {
    //DashBoard
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var btnDrop : UIButton!
    
}
