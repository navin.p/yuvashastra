//
//  MyAnswerVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/2/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class MyAnswerVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    var aryList = NSMutableArray()
    var refresher = UIRefreshControl()
    // MARK: - ----------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "My Answer"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.refresher.tintColor = hexStringToUIColor(hex: primaryBlueColor)
        self.tvlist!.addSubview(refresher)
        self.tvlist.reloadData()
     self.call_GetMyAnwerListData_API(tag: 1)
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        self.call_GetMyAnwerListData_API(tag: 2)
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_GetMyAnwerListData_API( tag : Int)  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            //----1
            if tag == 1{
                loading.startLoading(text: "Loading...")
            }
        
            let dictData = NSMutableDictionary()
            let urlForGetData = "\(API_GetAnswer)" + "UserId=\(dict_Login_Data.value(forKey: "UserId")!)" //
            WebService.callAPIBYGET(parameter: dictData, url: urlForGetData ) { (responce, status) in
                if tag == 1{
                    loading.endLoading()
                }else{
                    self.refresher.endRefreshing()
                }
                removeErrorView()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "userQuestionAnswerReportDc")as! NSArray)
                        self.aryList = NSMutableArray()
                        self.aryList = aryData.mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                        
                    }else{
                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "Success")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self, viewMaxY: Int((self.navigationController?.navigationBar.bounds.maxY)!))
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.call_GetMyAnwerListData_API(tag: 1)
    }

}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MyAnswerVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "MyAnswerCell", for: indexPath as IndexPath) as! MyAnswerCell
        let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        cell.lblQuestion.text = "Q. \(dict.value(forKey: "Question")!)"
        cell.lblPostedBy.text = "Posted By : \(dict.value(forKey: "PostedBy")!)\nPosted on : \(dateStringToFormatedDateString(dateToConvert: dict.value(forKey: "CreatedDate") as! String, dateFormat: "dd/MM/yyy"))"
        var arytemp = NSArray()
        arytemp = dict.value(forKey: "userAnswerReportDc")as! NSArray
        if(arytemp.count != 0){
            let dict = removeNullFromDict(dict: (arytemp.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            cell.lblAns.text = "Ans. \(dict.value(forKey: "Answer")!)"
            cell.lblAnsPostedby.text = "Posted By : \(dict.value(forKey: "PostedBy")!)\nPosted on : \(dateStringToFormatedDateString(dateToConvert: dict.value(forKey: "CreatedDate") as! String, dateFormat: "dd/MM/yyy"))"
            arytemp.count != 1 ? (cell.lblViewMore.text = "View More..") : (cell.lblViewMore.text = "")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var arytemp = NSArray()
        let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        arytemp = dict.value(forKey: "userAnswerReportDc")as! NSArray
        if(arytemp.count  > 1){
            let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyAnswerDetailVC")as! MyAnswerDetailVC
            testController.dictAnswerData = dict
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
    }

    
    
}

// MARK: - ----------------ChatCell
// MARK: -
class MyAnswerCell: UITableViewCell {
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblPostedBy: UILabel!
    @IBOutlet weak var lblAns: UILabel!
    @IBOutlet weak var lblAnsPostedby: UILabel!
    @IBOutlet weak var lblViewMore: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
