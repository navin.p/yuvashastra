//
//  AreaofExpertiseVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 3/28/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class AreaofExpertiseVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var btnNext: UIButton!
    
    var ary_for_Area = NSMutableArray()
    var ary_for_AreaSelected = NSMutableArray()
    var ary_for_Send = NSMutableArray()
    
    var refresher = UIRefreshControl()
    var dictRegisterData = NSMutableDictionary()
    var imgProfile = UIImage()
    
    // MARK: - ---------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Area Of Expertise"

        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        btnNext.layer.borderColor = UIColor.darkGray.cgColor
        btnNext.layer.borderWidth = 1.0
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
        if(dataFromLocal(strEntity: "AreaofExpertise", strkey: "areaofExpertise").count != 0){
            self.ary_for_Area = NSMutableArray()
            self.ary_for_Area = (dataFromLocal(strEntity: "AreaofExpertise", strkey: "areaofExpertise")as NSArray).mutableCopy()as! NSMutableArray
            self.tvlist.reloadData()
        }else{
            self.call_GetCategoryDropdown_API(tag: 1)
        }
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        self.call_GetCategoryDropdown_API(tag: 2)
    }
    // MARK: - ---------------IBAction
    // MARK: -

    @IBAction func actiononNext(_ sender: UIButton) {
        if(ary_for_AreaSelected.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAreaexpertise, viewcontrol: self)
        }else{
            ary_for_Send = NSMutableArray()
            for item in ary_for_AreaSelected{
                let dict  = NSMutableDictionary()
                dict.setValue("0", forKey: "UserCategoryId")
                dict.setValue("0", forKey: "UserId")
                dict.setValue("true", forKey: "IsActive")
                dict.setValue("false", forKey: "IsDeleted")
                dict.setValue("\((item as AnyObject).value(forKey: "CategoryId")!)", forKey: "CategoryId")
                dict.setValue("\((item as AnyObject).value(forKey: "CategoryName")!)", forKey: "CategoryName")
                ary_for_Send.add(dict)
            }
            self.dictRegisterData.setValue(ary_for_Send, forKey: "userCategoryMappingDc")
            
            if(dictRegisterData.value(forKey: "ProfileImage")as! String != ""){
               uploadProfileImage()
            }else{
                
                RegisterApiCalling(sender: sender)
            }
        }
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self, viewMaxY: Int((self.navigationController?.navigationBar.bounds.maxY)!))
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.call_GetCategoryDropdown_API(tag: 1)
    }
    // MARK: - ---------------API's Calling
    // MARK: -

    
    func uploadProfileImage()  {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

            WebService.callAPIWithImage(parameter: NSDictionary(), url: BaseURLImageUpload, image: imgProfile, fileName: dictRegisterData.value(forKey: "ProfileImage")as! String, withName: "userfile") { (responce, status) in
                FTIndicator.dismissProgress()

                if(status == "Suceess"){
                    self.RegisterApiCalling(sender: UIButton())
                }
            }
        }
    }
    
    func RegisterApiCalling(sender : UIButton){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            let urlForGetData = "\(API_Register)" + "RegistrationDc"
            WebService.postRequestWithHeaders(dictJson: dictRegisterData, url: urlForGetData) { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVarifyVC")as! OTPVarifyVC
                        testController.dictData = self.dictRegisterData
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
        }
    }
    
    func call_GetCategoryDropdown_API( tag : Int)  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)
            
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            if tag == 1{
                loading.startLoading(text: "Loading...")
            }
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: API_GetCategoryDropdown) { (responce, status) in
                if tag == 1{
                    loading.endLoading()
                }else{
                    self.refresher.endRefreshing()
                }
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "categoryDropdownDc")as! NSArray)
                        self.ary_for_Area = NSMutableArray()
                        self.ary_for_Area = aryData.mutableCopy()as! NSMutableArray
                        deleteAllRecords(strEntity:"AreaofExpertise")
                        saveDataInLocalArray(strEntity: "AreaofExpertise", strKey: "areaofExpertise", data: self.ary_for_Area)
                    }else{
                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "Success")as! String)", imgError: strDataNotFoundImage)
                        
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    // MARK: - --------------Local Data Base
    // MARK: -
    func dataFromLocal(strEntity: String ,strkey : String )-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: strEntity, strkey: strkey)
        
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: strkey) ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AreaofExpertiseVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ary_for_Area.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "viewAreaCell", for: indexPath as IndexPath) as! viewAreaCell
        
        let dict = ary_for_Area.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "CategoryName")!)"
        cell.btnCheck.tag = indexPath.row
        (ary_for_AreaSelected.contains(dict)) != true ? cell.btnCheck.setImage(UIImage(named: "uncheck"), for: .normal) : cell.btnCheck.setImage(UIImage(named: "check"), for: .normal)
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = ary_for_Area.object(at: indexPath.row)as! NSDictionary
        (ary_for_AreaSelected.contains(dict)) == true ? ary_for_AreaSelected.remove(dict) : ary_for_AreaSelected.add(dict)
        tvlist.reloadData()
        
    }
    
}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class viewAreaCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
