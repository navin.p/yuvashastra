//
//  MentorProfileVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/4/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class MentorProfileDetailVC: UIViewController {
   @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var Category: UILabel!
    @IBOutlet weak var CurruntPosition: UILabel!
    @IBOutlet weak var MobileNumber: UILabel!
    @IBOutlet weak var Name: UILabel!

   
   var dictData = NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Profile"
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imgProfile.layer.borderWidth = 1.0
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        imgProfile.clipsToBounds = true
        imgProfile.layer.borderColor = UIColor.white.cgColor
        Name.text = "Name : \(dictData.value(forKey: "Name")!)"
        CurruntPosition.text = "Position : \(dictData.value(forKey: "CurruntPosition")!)"
        Category.text = "Expertise : \(dictData.value(forKey: "Category")!)"
        MobileNumber.text = ""
        let urlImage = "\(BaseURLImageDownload)\(dictData.value(forKey: "ProfileImage")!)"
       imgProfile.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "no-image"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
        self.imgProfile.isUserInteractionEnabled = true
        self.imgProfile.addGestureRecognizer(tapGestureRecognizer)
        }, usingActivityIndicatorStyle: .gray)
    }

  
    @objc func connected(_ sender:AnyObject){
        let dict = removeNullFromDict(dict: dictData)
        let urlImage = "\(BaseURLImageDownload)\(dict.value(forKey: "ProfileImage")!)"
        let imgView = UIImageView()
        var strImagePlaceholder = "mentor_img"
        if(dict_Login_Data.value(forKey: "Role")as! String == "Student"){
            strImagePlaceholder = "aspirant_img"
        }
        imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: strImagePlaceholder), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
            testController.img = imgView.image!
            self.navigationController?.pushViewController(testController, animated: true)
        }, usingActivityIndicatorStyle: .gray)
    }

}
