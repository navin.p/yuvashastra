//
//  MentorChatVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 3/28/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class MentorChatVC: UIViewController {
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    var aryList = NSMutableArray()
    var aryForListData = NSMutableArray()
   var strErrorMessage = String()
    var intNumberofRow = 0

    override func viewDidLoad() {
        
        super.viewDidLoad()
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refresh))
        navigationItem.rightBarButtonItems = [refreshButton]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.call_GetMentorListData_API(tag: 1)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.prefersLargeTitles = true

    }
    // MARK: - ------------More
    // MARK: -
    @objc func refresh(){
        self.view.endEditing(true)
        self.call_GetMentorListData_API(tag: 1)

    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_GetMentorListData_API( tag : Int)  {
        if !(isInternetAvailable()){
            strErrorMessage = alertInternet
            intNumberofRow = 1
            self.tvlist.reloadData()
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
             loading.startLoading(text: "Loading...")
            let dictData = NSMutableDictionary()
           var aryCategoryId = NSMutableArray()
            var strCategoryID = ""
            var strRole = "Student"

            aryCategoryId = (dict_Login_Data.value(forKey: "userCategoryMappingDc")as! NSArray).mutableCopy()as! NSMutableArray
            for item in aryCategoryId{
                strCategoryID = strCategoryID + "\((item as AnyObject).value(forKey: "CategoryId")!),"
            }
            if(strCategoryID.count > 0){
                strCategoryID = String(strCategoryID.dropLast())
            }
            if("\(dict_Login_Data.value(forKey: "Role")!)" == "Student"){
                strRole = "Mentor"
            }
            let urlForGetData = "\(API_GetMember)" + "CategoryIds=\(strCategoryID)"  + "&Role=\(strRole)"
            
            WebService.callAPIBYGET(parameter: dictData, url: urlForGetData ) { (responce, status) in
                    loading.endLoading()
                    removeErrorView()
                
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "UserChatBoardReportDc")as! NSArray)
                        self.aryList = NSMutableArray()
                        self.aryList = aryData.mutableCopy()as! NSMutableArray
                        self.aryForListData = NSMutableArray()
                        self.aryForListData = aryData.mutableCopy()as! NSMutableArray
                    }else{
                        self.strErrorMessage = "\(dict.value(forKey: "Success")as! String)"
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                }
                self.intNumberofRow = 1
                self.tvlist.reloadData()
            }
        }
     
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MentorChatVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count == 0 ? intNumberofRow : aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if( aryList.count != 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath as IndexPath) as! ChatCell
            let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            cell.lblTitle.text = "\(dict.value(forKey: "Name")!)"
            if("\(dict_Login_Data.value(forKey: "Role")!)" == "Student"){
                cell.lblSubTitle.text = "Mentor"
            }else{
                cell.lblSubTitle.text = "Student"
            }
            cell.imgView.layer.borderWidth = 1.0
            cell.imgView.layer.masksToBounds = false
            cell.imgView.layer.cornerRadius = cell.imgView.frame.height/2
            cell.imgView.clipsToBounds = true
            cell.imgView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            let urlImage = "\(BaseURLImageDownload)\(dict.value(forKey: "ProfileImage")!)"
            cell.imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "no-image"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                //print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            return cell
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ChatCellError", for: indexPath as IndexPath) as! ChatCell
            cell.lblErrorTitle.text = strErrorMessage
            return cell
        }
  
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return aryList.count != 0 ? 110 : self.tvlist.frame.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if( aryList.count != 0){
            let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ChatDetailVC")as! ChatDetailVC
            testController.dictData = dict
            self.navigationController?.pushViewController(testController, animated: true)
        }else{
            intNumberofRow = 0
             tvlist.reloadData()
             self.call_GetMentorListData_API(tag: 1)
        }
    }
  
}

// MARK: - ----------------ChatCell
// MARK: -
class ChatCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblErrorTitle: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - -------------UISearchBarDelegate
// MARK: -
extension  MentorChatVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        
        let resultPredicate = NSPredicate(format: "Name contains[c] %@ OR Name contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryForListData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryForListData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        if(aryList.count == 0){
        }
    }
}
