//
//  CommentVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/3/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class CommentVC: UIViewController {
    
    var dictData = NSMutableDictionary()
    var aryList = NSMutableArray()
    var refresher = UIRefreshControl()
    var questionID = String()
    var userID = String()
    var strComeFromView = String()

    var strErrorMessage = String()
    var intNumberofRow = 0
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var textField: KMPlaceholderTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Comments"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.refresher.tintColor = hexStringToUIColor(hex: primaryBlueColor)
        self.tvlist!.addSubview(refresher)
        self.tvlist.reloadData()
        self.tvlist.scrollToBottom(animated: true)
        
        if(strComeFromView == "Question"){
            questionID = "\(dictData.value(forKey: "QuestionId")!)"
        }else{
            questionID = "\(dictData.value(forKey: "AnswerId")!)"
        }
        userID = "\(dict_Login_Data.value(forKey: "UserId")!)"
       self.call_CommentData_API(tag: 1)
    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        self.call_CommentData_API(tag: 2)
    }
    @IBAction func sendText(_ sender: UIButton) {
        if(textField.text.count != 0){
            AddCommentApiCalling(sender: sender)
        }
    }
    
    // MARK: - ---------------API's Calling
    // MARK: -
    
    
    func AddCommentApiCalling(sender : UIButton){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            var baseUrl = ""
            if(strComeFromView == "Question"){
                baseUrl =  "\(API_AddQuestionLikesComments)" + "QuestionId=\(questionID)"  + "&UserId=\(userID)" + "&Type=Comment" + "&Comment=\(textField.text!)"
            }else{
                baseUrl =  "\(API_AddAnswerLikesComments)" + "AnswerId=\(questionID)"  + "&UserId=\(userID)" + "&Type=Comment" + "&Comment=\(textField.text!)"
            }
            print(baseUrl)
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: baseUrl) { (responce, status) in
                FTIndicator.dismissProgress()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.textField.text = ""
                        self.call_CommentData_API(tag: 1)
                    }else{
                      showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_CommentData_API( tag : Int)  {
        if !(isInternetAvailable()){
            self.strErrorMessage =  alertInternet
            intNumberofRow = 1
            self.tvlist.reloadData()
            
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            if tag == 1{
                loading.startLoading(text: "Loading Comments...")
            }
            let dictDatasend = NSMutableDictionary()
            
            var baseUrl = ""
            if(strComeFromView == "Question"){
                baseUrl =  "\(API_GetQuestionLikesComments)" + "QuestionId=\(questionID)"  + "&Type=Comment"
            }else{
              baseUrl =  "\(API_GetAnswerLikesComments)" + "AnswerId=\(questionID)"  + "&Type=Comment"
            }
            WebService.callAPIBYGET(parameter: dictDatasend, url: baseUrl ) { (responce, status) in
                if tag == 1{
                    loading.endLoading()
                }else{
                    self.refresher.endRefreshing()
                }
                removeErrorView()
                print(responce)

                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        
                        let aryData  = (dict.value(forKey: "\(self.strComeFromView != "Question" ? "AnswerLikeCommentDc" : "QuestionLikeCommentDc")")as! NSArray)
                        self.aryList = NSMutableArray()
                        self.aryList = aryData.mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                        self.tvlist.scrollToBottom(animated: true)

                    }else{
                        self.strErrorMessage =  "\(dict.value(forKey: "Success")as! String)"
                    }
                }else{
                    self.strErrorMessage =  alertDataNotFound
                }
                self.intNumberofRow = 1
                self.tvlist.reloadData()
            }
        }
    }
  
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension CommentVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count != 0 ? aryList.count : intNumberofRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(aryList.count != 0){
            let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            let msg =  "\(dict.value(forKey: "Comment")!)"
            let name =  "Comment by :\(dict.value(forKey: "Name")!)"
            
            if("\(dict.value(forKey: "UserId")!)" != userID){
                let cell = tvlist.dequeueReusableCell(withIdentifier: "RecieverCell", for: indexPath as IndexPath) as! CommentCell
                cell.lblMessage.text = "\n" + "  " + msg  + "   " + "\n"
                cell.lblMessage.layer.cornerRadius = 12.0
                cell.lblMessage.layer.masksToBounds = true
                cell.lblDate.text = name
                return cell
                
            }else{ // // Self
                let cell = tvlist.dequeueReusableCell(withIdentifier: "SenderCell", for: indexPath as IndexPath) as! CommentCell
                cell.lblMessage.text =  "\n" + "  " + msg  + "   " + "\n"
                cell.lblDate.text = name
                
                cell.lblMessage.layer.cornerRadius = 12.0
                cell.lblMessage.layer.masksToBounds = true
                
                return cell
        }
   
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ChatCellError", for: indexPath as IndexPath) as! ChatCell
            cell.lblErrorTitle.text = strErrorMessage
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return aryList.count != 0 ? UITableView.automaticDimension : self.tvlist.frame.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if( aryList.count == 0){
            intNumberofRow = 0
            self.tvlist.reloadData()
            self.call_CommentData_API(tag: 1)
        }
    }
}

// MARK: - ----------------ChatCell
// MARK: -
class CommentCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ----------------UITextViewDelegate
// MARK: -
extension CommentVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return txtViewValidation(textField: textView, string: text, returnOnly: "All", limitValue: 9999999999999)
        
    }
}


