//
//  NewsVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/2/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class NewsVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    var aryList = NSMutableArray()
    var refresher = UIRefreshControl()
    var strErrorMessage = String()
    var intNumberofRow = 0
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "News"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.refresher.tintColor = hexStringToUIColor(hex: primaryBlueColor)
        self.tvlist!.addSubview(refresher)
        self.tvlist.reloadData()
 self.call_GetNewsListData_API(tag: 1)
    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        self.call_GetNewsListData_API(tag: 2)
    }
    
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_GetNewsListData_API( tag : Int)  {
        if !(isInternetAvailable()){
            strErrorMessage = alertInternet
            intNumberofRow = 1
            self.tvlist.reloadData()
            
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            //----1
            if tag == 1{
                loading.startLoading(text: "Loading...")
            }
            //---2
            let dictData = NSMutableDictionary()
            let urlForGetData = "\(API_GetNews)" + "city_id=\(dict_Login_Data.value(forKey: "city_id")!)"
            
            WebService.callAPIBYGET(parameter: dictData, url: urlForGetData ) { (responce, status) in
                if tag == 1{
                    loading.endLoading()
                }else{
                    self.refresher.endRefreshing()
                }
                removeErrorView()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "NewsNotificationDc")as! NSArray)
                        self.aryList = NSMutableArray()
                        self.aryList = aryData.mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                        
                    }else{
                     self.strErrorMessage = "\(dict.value(forKey: "Success")as! String)"
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                }
                self.intNumberofRow = 1
                self.tvlist.reloadData()
            }
        }
    }
 

}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension NewsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count == 0 ? intNumberofRow : aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if( aryList.count == 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ChatCellError", for: indexPath as IndexPath) as! ChatCell
            cell.lblErrorTitle.text = strErrorMessage
            return cell
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath as IndexPath) as! NewsCell
            let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            cell.lblnewsTitle.text = "\(dict.value(forKey: "Title")!)"
            cell.lblPostedBy.text = "Posted by : \(dict.value(forKey: "CreatedBy")!)"
            cell.lblDate.text = "Posted on :" + dateStringToFormatedDateString(dateToConvert: "\(dict.value(forKey: "CreatedDate")!)", dateFormat: "dd/MM/yyy hh:mm a")
            cell.lblDate.textColor = hexStringToUIColor(hex: primaryBlueColor)
            cell.lblPostedBy.textColor = hexStringToUIColor(hex: primaryBlueColor)
            
            cell.lblNews.text = "\(dict.value(forKey: "Description")!)"
            cell.imgView.layer.borderWidth = 1.0
            cell.imgView.layer.masksToBounds = false
            cell.imgView.layer.cornerRadius = 10.0
            cell.imgView.clipsToBounds = true
            cell.imgView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            let urlImage = "\(BaseURLImageDownload)\(dict.value(forKey: "ImagePath")!)"
            cell.imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "no-image"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                //print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            return cell
        }
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return aryList.count != 0 ? UITableView.automaticDimension : self.tvlist.frame.height

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if( aryList.count == 0){
            intNumberofRow = 0
            tvlist.reloadData()
            self.call_GetNewsListData_API(tag: 1)
        }
    }
    

    
}

// MARK: - ----------------ChatCell
// MARK: -
class NewsCell: UITableViewCell {
    @IBOutlet weak var lblnewsTitle: UILabel!
    @IBOutlet weak var lblPostedBy: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNews: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
