//
//  AddAnswerVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/3/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit
import MobileCoreServices
class AddAnswerVC: UIViewController {
    // MARK:
    // MARK: - IBOutlet
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtview: KMPlaceholderTextView!
    @IBOutlet weak var imgProfile: UIImageView!
    var imagePicker = UIImagePickerController()
    var strProfileImageName = ""
    var strPdfName = ""
    var pdfData = Data()
    var dictQuestion = NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Add Answer"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        btnNext.layer.borderColor = UIColor.lightGray.cgColor
        btnNext.layer.borderWidth = 1.0
        imgProfile.contentMode = .scaleToFill
        
        txtview.text =  ""
    }

// MARK:
// MARK: - IBAction
    @IBAction func actionOnDocument(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: Alert_SelectionMessage, message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            if(UIImagePickerController .isSourceTypeAvailable(.camera)){
                self.imagePicker.sourceType = .camera
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)
            } else {
            }
        }))
        
        alert.addAction(UIAlertAction(title: Alert_pdf, style: .default , handler:{ (UIAlertAction)in
            let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            documentPicker.delegate = self
            self.present(documentPicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
      
        
        self.present(alert, animated: true, completion: {
        })
    }
@IBAction func actionOnSubmitBottom(_ sender: UIButton) {
    self.view.endEditing(true)
    if txtview.text?.count == 0
    {
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Answer required!", viewcontrol: self)
    }
    else{
        if(strProfileImageName != ""){
            uploadDocumentImage()
        }
        else if(strPdfName != ""){
            uploadPdf()
            
        }
        else{
            self.AddAnswerApiCalling(sender: sender)
        }
    }
}
// MARK:
// MARK: - API Calling
    func AddAnswerApiCalling(sender : UIButton){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            let dictAnswer = NSMutableDictionary()
            
            let dictattech = NSMutableDictionary()
            dictattech.setValue("0", forKey: "CreatedDate")
            if(strProfileImageName != ""){
                dictattech.setValue("image", forKey: "AttachmentType")
                dictattech.setValue(strProfileImageName, forKey: "AttachmentPath")

            }
            else if(strPdfName != ""){
                dictattech.setValue("pdf", forKey: "AttachmentType")
                dictattech.setValue(strPdfName, forKey: "AttachmentPath")

            }else{
                dictattech.setValue("", forKey: "AttachmentType")
                dictattech.setValue("", forKey: "AttachmentPath")

            }
            dictattech.setValue("0", forKey: "AttachmentId")
            dictattech.setValue("0", forKey: "AnswerId")
            let aryAttech = NSMutableArray()
            aryAttech.add(dictattech)
            
            dictAnswer.setValue("0", forKey: "AnswerId")
            dictAnswer.setValue("", forKey: "CreatedDate")
            dictAnswer.setValue("\(dictQuestion.value(forKey: "QuestionId")!)", forKey: "QuestionId")
            dictAnswer.setValue("\(dict_Login_Data.value(forKey: "UserId")!)", forKey: "PostedByUserId")
            dictAnswer.setValue(txtview.text!, forKey: "Answer")
            dictAnswer.setValue("true", forKey: "IsActive")
            dictAnswer.setValue("false", forKey: "IsDeleted")
            dictAnswer.setValue("\(dict_Login_Data.value(forKey: "Name")!)", forKey: "PostedBy")
            dictAnswer.setValue(aryAttech, forKey: "attachmentTableDc")
            let urlForGetData = "\(API_AddAnswer)" + "AnswerTableDc="
            WebService.postRequestWithHeaders(dictJson: dictAnswer, url: urlForGetData) { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        FTIndicator.showToastMessage("\(dict.value(forKey: "Success")as! String)")
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    
    func uploadDocumentImage()  {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            
            WebService.callAPIWithImage(parameter: NSDictionary(), url: BaseURLDocumentImageUpload, image: imgProfile.image!, fileName: strProfileImageName, withName: "userfile") { (responce, status) in
                FTIndicator.dismissProgress()
                
                if(status == "Suceess"){
                    self.AddAnswerApiCalling(sender: UIButton())
                }
            }
        }
    }
    func uploadPdf()  {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            
            WebService.callAPIWithpdf(parameter: NSDictionary(), url: BaseURLDocumentImageUpload, pdf: pdfData, fileName: strPdfName, withName: "userfile") { (responce, status) in
                FTIndicator.dismissProgress()
                
                if(status == "Suceess"){
                    self.AddAnswerApiCalling(sender: UIButton())
                }
            }
        }
    }
}

// MARK:
// MARK: - UITextViewDelegate
extension AddAnswerVC : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return txtViewValidation(textField: textView, string: text, returnOnly: "ALL", limitValue: 500)
    }
}
// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -

extension AddAnswerVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            self.imgProfile.contentMode = .scaleAspectFill
            self.imgProfile.image = image
            self.strProfileImageName = getUniqueString()
            self.strPdfName = ""
            self.dismiss(animated: false) {
            }
        } else {
            self.dismiss(animated: false) {
            }
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  false, completion: nil)
    }
}
// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension AddAnswerVC: UIDocumentPickerDelegate{
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let cico = url as URL
        print(cico)
        print(url)
        print(url.lastPathComponent)
        print(url.pathExtension)
         self.strProfileImageName = ""
        self.imgProfile.image = UIImage(named: "pdf")
        do {
           self.pdfData = try Data(contentsOf: url as URL)
            self.strPdfName = getUniqueString().replacingOccurrences(of: ".jpg", with: ".pdf")
        } catch {
            print("Unable to load data: \(error)")
        }
    }
}
