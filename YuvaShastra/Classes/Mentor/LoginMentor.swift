//
//  LoginMentor.swift
//  YuvaShastra
//
//  Created by Rakesh Jain on 11/03/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit



class LoginMentor: UIViewController
{
    // MARK: -
    // MARK: - Global Variables
    var arrQualification = NSMutableArray()
    var dictQualification = NSMutableDictionary()
    var strComeFrom = String()
    // MARK:
    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var constLblLine_L: NSLayoutConstraint!
    @IBOutlet var viewPicker: UIView!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var btnDone: UIButton!
    
    // MARK:
    // MARK: - View Login Outlets
    
    @IBOutlet var viewLogin: CardView!
    @IBOutlet weak var txtLoginMobileNo: ACFloatingTextfield!
    @IBOutlet weak var txtLoginPassword: ACFloatingTextfield!
    @IBOutlet weak var btnKeepMeLogin: UIButton!
    @IBOutlet weak var btnShowPwd: UIButton!
    @IBOutlet weak var btnLoginForgotPassword: UIButton!
    @IBOutlet weak var btnLoginBottom: UIButton!
    
    // MARK:
    // MARK: - View Register Outlets
    
    @IBOutlet var viewRegister: CardView!
    @IBOutlet weak var txtRegisterFullName: ACFloatingTextfield!
    @IBOutlet weak var txtRegisterDOB: ACFloatingTextfield!
    @IBOutlet weak var btnRegisterDOB: UIButton!
    @IBOutlet weak var txtRegisterQualification: ACFloatingTextfield!
    @IBOutlet weak var txtRegisterCurrentPosition: ACFloatingTextfield!
    @IBOutlet weak var txtRegisterAwardsRecognition: ACFloatingTextfield!
    @IBOutlet weak var txtRegisterMobileNo: ACFloatingTextfield!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnRegisterCheckBox: UIButton!
    @IBOutlet weak var btnRegisterQualification: UIButton!
    
  
    
  // MARK: -
    // MARK: - Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btnLoginBottom.layer.borderColor = UIColor.darkGray.cgColor
        btnLoginBottom.layer.borderWidth = 1.0
        btnNext.layer.borderColor = UIColor.darkGray.cgColor
        btnNext.layer.borderWidth = 1.0
        btnShowPwd.setBackgroundImage(UIImage(named: "hidePassword"), for: .normal)
        addLoginView()
//        txtLoginMobileNo.text = "9685730669"
//        txtLoginPassword.text = "123456"
        setUpNavBar(strtilte: "")
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(self.view.tag == 99){
             addLoginView()
        }
    }
    func setUpNavBar(strtilte : String){
    //    self.navigationItem.title = strtilte
        let logo = UIImage(named: "toolbar_logo")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
    }
    // MARK:
    // MARK: - Actions
   
  
    // MARK: - Login Actions
    @IBAction func actionOnShowPassword(_ sender: Any)
    {
        btnShowPwd.currentBackgroundImage == (UIImage(named: "hidePassword")) ? (txtLoginPassword.isSecureTextEntry = false) : (txtLoginPassword.isSecureTextEntry = true)
        
        btnShowPwd.currentBackgroundImage == (UIImage(named: "hidePassword")) ? btnShowPwd.setBackgroundImage(UIImage(named: "showPassword"), for: .normal) : self.btnShowPwd.setBackgroundImage(UIImage(named: "hidePassword"), for: .normal)
        
    }
    @IBAction func actionOnLogin(_ sender: Any)
    {
        self.view.endEditing(true)
        constLblLine_L.constant = 0
        UIView.animate(withDuration: 0.3)
        {
            self.view.layoutIfNeeded()
        }
        viewRegister.removeFromSuperview()
        viewLogin.shadowColor = UIColor.clear
        viewLogin.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewLogin.frame.height)
        scrollView.addSubview(viewLogin)
        scrollView.contentSize = CGSize (width: scrollView.frame.width, height: viewLogin.frame.height)
    }
    @IBAction func actionKeepMeLogin(_ sender: Any)
    {
        self.view.endEditing(true)

       btnKeepMeLogin.currentImage == (UIImage(named: "uncheck")) ? self.btnKeepMeLogin.setImage(UIImage(named: "check"), for: .normal) : self.btnKeepMeLogin.setImage(UIImage(named: "uncheck"), for: .normal)
    
    }
    
    @IBAction func actionForgotPassword(_ sender: UIButton) {
        self.view.endEditing(true)
         viewForgotAlert()
    }
    @IBAction func actionOnloginBottom(_ sender: UIButton) {
        self.view.endEditing(true)

        if txtLoginMobileNo.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_MobileNumber, viewcontrol: self)
        }else if (txtLoginMobileNo.text?.count)! < 10
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_MobileNumberLimit, viewcontrol: self)
        }
        else if txtLoginPassword.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_Password, viewcontrol: self)
        }
        else{
        self.LoginApiCalling(sender: sender)
        }
    }
    // MARK: - Register Actions
    
    @IBAction func actionOnRegister(_ sender: Any)
    {
        self.view.endEditing(true)

        constLblLine_L.constant = btnLogin.frame.width
        UIView.animate(withDuration: 0.3)
        {
            self.view.layoutIfNeeded()
        }
        viewLogin.removeFromSuperview()
        viewRegister.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewRegister.frame.height)
        viewRegister.shadowColor = UIColor.clear
        scrollView.addSubview(viewRegister)
        scrollView.contentSize = CGSize (width: scrollView.frame.width, height: viewRegister.frame.height)
        txtRegisterFullName.text = ""
        txtRegisterDOB.text = ""
        txtRegisterCurrentPosition.text = ""
        txtRegisterQualification.text = ""
        txtRegisterAwardsRecognition.text = ""
        txtRegisterMobileNo.text = ""
    }
    
    @IBAction func actionOnDOB(_ sender: UIButton)
    {
        self.view.endEditing(true)

        self.viewPicker.frame = self.view.frame
        picker.maximumDate = Date()
        self.view.addSubview(self.viewPicker)
    }
    @IBAction func actionOnPickerDone(_ sender: Any)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let somedateString = dateFormatter.string(from: picker.date)
        txtRegisterDOB.text = somedateString
        self.viewPicker.removeFromSuperview()
    }
    @IBAction func actionOnPickerClose(_ sender: Any)
    {
        self.viewPicker.removeFromSuperview()
    }
    
    @IBAction func actionOnQualification(_ sender: UIButton)
    {
        self.view.endEditing(true)
        sender.tag = 1
        gotoPopViewWithArray(sender: sender, aryData: arrQualification, strTitle: "--- Select ---")
    }
    
    @IBAction func actionRegisterCheckBox(_ sender: Any)
    {
        self.view.endEditing(true)

         btnRegisterCheckBox.currentImage == (UIImage(named: "uncheck")) ? self.btnRegisterCheckBox.setImage(UIImage(named: "check"), for: .normal) :  self.btnRegisterCheckBox.setImage(UIImage(named: "uncheck"), for: .normal)
    }
    
    
    @IBAction func actionOnNext(_ sender: UIButton)
    {
        self.view.endEditing(true)

        if txtRegisterFullName.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Full name required", viewcontrol: self)
        }
        else if txtRegisterDOB.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "DOB required", viewcontrol: self)
        }
        else if txtRegisterQualification.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Qualification required", viewcontrol: self)
        }
        else if txtRegisterCurrentPosition.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Current position required", viewcontrol: self)
        }
        else if txtRegisterMobileNo.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_MobileNumber, viewcontrol: self)
        }
        else if (txtRegisterMobileNo.text?.count)! < 10
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_MobileNumberLimit, viewcontrol: self)
        }
        else if (btnRegisterCheckBox.currentImage == UIImage(named: "uncheck"))
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please check terms & condition.", viewcontrol: self)
        }
        else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "SetMentorProfileVC")as! SetMentorProfileVC
            let dictData = NSMutableDictionary()
            dictData.setValue("0", forKey: "UserId")
            dictData.setValue("1", forKey: "city_id")
            dictData.setValue(txtRegisterFullName.text!, forKey: "Name")
            dictData.setValue(txtRegisterMobileNo.text!, forKey: "MobileNumber")
            dictData.setValue("", forKey: "EmailId")
            dictData.setValue(txtRegisterMobileNo.text!, forKey: "UserName")
            dictData.setValue("", forKey: "password")
            dictData.setValue(txtRegisterDOB.text!, forKey: "DOB")
            dictData.setValue("Mentor", forKey: "Role")

            dictData.setValue("YuvaShastra", forKey: "SocialMediaType")
            dictData.setValue(txtRegisterCurrentPosition.text!, forKey: "CurruntPosition")
            dictData.setValue(txtRegisterAwardsRecognition.text!, forKey: "AwardRecognition")
            dictData.setValue("", forKey: "ProfileImage")
            dictData.setValue("true", forKey: "IsApproveMentor")
            dictData.setValue("true", forKey: "IsActive")
            dictData.setValue("false", forKey: "IsDeleted")
            dictData.setValue("", forKey: "CreatedDate")
            dictData.setValue("", forKey: "userCategoryMappingDc")
            let aryquali = NSMutableArray()
            dictQualification.setValue("0", forKey: "UserQualificationId")
            dictQualification.setValue("0", forKey: "UserId")
            dictQualification.setValue("", forKey: "PassingYear")
            dictQualification.setValue("1", forKey: "PassingDivision")
            dictQualification.setValue("", forKey: "ScorePer")
            dictQualification.setValue("true", forKey: "IsActive")
            dictQualification.setValue("false", forKey: "IsDeleted")
            aryquali.add(dictQualification)
            dictData.setValue(aryquali, forKey: "userQualificationMappingDc")
            testController.dictRegisterData = dictData
            self.navigationController?.pushViewController(testController, animated: true)
        }
  }
    // MARK:
    // MARK: - Extra Function
    func addLoginView()  {
        constLblLine_L.constant = 0
        viewLogin.shadowColor = UIColor.clear
        viewLogin.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewLogin.frame.height)
        scrollView.addSubview(viewLogin)
        scrollView.contentSize = CGSize (width: scrollView.frame.width, height: viewLogin.frame.height)
        call_GetQualificationDropdown_API()
        txtLoginMobileNo.text = ""
        txtLoginPassword.text = ""
    }
    
    
    func viewForgotAlert()  {
        let alertController = UIAlertController(title: "Forgot Password", message: "Enter the registrered mobile number associated with your  account in the text box and then click the Submit  button.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Submit", style: .default, handler: { alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            textField.keyboardType = .numberPad
            if textField.text!.count < 10{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_MobileNumberLimit, viewcontrol: self)
                self.viewForgotAlert()
            }else{
                self.callForgotPasswordAPI(sender: textField.text!)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Mobile Number"
            textField.borderStyle = UITextField.BorderStyle.roundedRect
            textField.delegate = self
            textField.tag = 99
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK:
    // MARK: - API Calling
    func
        LoginApiCalling(sender : UIButton){
        if !(isInternetAvailable()){
              FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

            let urlForGetData = "\(API_GetLogin)" + "username=\(self.txtLoginMobileNo.text!)" + "&password=\(self.txtLoginPassword.text!)"
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: urlForGetData) { (responce, status) in
                   FTIndicator.dismissProgress()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.btnKeepMeLogin.currentImage == UIImage(named: "check") ? nsud.setValue("true", forKey: "Yuva_LoginRemberStatus") :  nsud.setValue("false", forKey: "Yuva_LoginRemberStatus")
                        nsud.setValue(removeNullFromDict(dict: (dict.value(forKey: "RegistrationDc")as! NSDictionary).mutableCopy()as! NSMutableDictionary), forKey: "Yuva_LoginData")
                        deleteAllRecords(strEntity:"LoginData")
                        let aryTemp = NSMutableArray()
                        aryTemp.add(removeNullFromDict(dict: (dict.value(forKey: "RegistrationDc")as! NSDictionary).mutableCopy()as! NSMutableDictionary))
                        saveDataInLocalArray(strEntity: "LoginData", strKey: "loginData", data: aryTemp)
                        
                        if ("\(((dict.value(forKey: "RegistrationDc")as! NSDictionary).value(forKey: "IsVerify")!))" == "1"){
                            self.navigationController?.setNavigationBarHidden(true, animated: true)

                            let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }else{
                            let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVarifyVC") as! OTPVarifyVC
                            nextViewController.strViewComeFrom = "Login"
                            let dict = NSMutableDictionary()
                            dict.setValue(self.txtLoginMobileNo.text!, forKey: "UserName")
                            nextViewController.dictData = dict
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_GetQualificationDropdown_API( )  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: API_GetQualificationDropdown) { (responce, status) in
                FTIndicator.dismissProgress()

                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "qualificationDropdownDc")as! NSArray)
                        self.arrQualification = NSMutableArray()
                        self.arrQualification = aryData.mutableCopy()as! NSMutableArray
                    }else{
                    }
                }else{
                }
            }
        }
    }
    func callForgotPasswordAPI(sender : String) {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...")
            let urlForGetData = "\(API_ForgetPassword)" + "MobileNumber=\(sender)"
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: urlForGetData) { (responce, status) in
                FTIndicator.dismissProgress()
                
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)

                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)

                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            }
        }
    }
    // MARK:
    // MARK: - Other Function
   
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.aryTBL = aryData
            vc.delegate = self
            self.present(vc, animated: false, completion: {})
        }
    }


}
//MARK:-
//MARK:- ---------PopUpDelegate

extension LoginMentor : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 1){
            dictQualification =  NSMutableDictionary()
            
            dictQualification = dictData.mutableCopy()as! NSMutableDictionary
            self.txtRegisterQualification.text = "\(dictData.value(forKey: "Qualification")!)"
            self.txtRegisterQualification.tag = Int("\(dictData.value(forKey: "QualificationId")!)")!
        }
    }
}
//MARK:-
//MARK:- ---------TextField Extension

extension LoginMentor : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (  textField == txtRegisterFullName || textField == txtRegisterCurrentPosition || textField == txtRegisterAwardsRecognition)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "All", limitValue: 85)
        }
        else if textField == txtLoginMobileNo || textField == txtRegisterMobileNo
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        else if (  textField == txtLoginPassword)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "All", limitValue: 11)
        } else if (  textField.tag == 99)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        else
        {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
