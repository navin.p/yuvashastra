//
//  OTPVarifyVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/14/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class OTPVarifyVC: UIViewController {
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var tf1: UITextField!
    @IBOutlet weak var tf2: UITextField!
    @IBOutlet weak var tf3: UITextField!
    @IBOutlet weak var tf4: UITextField!
    
    @IBOutlet weak var lblShowCounter: UILabel!
    @IBOutlet weak var lblClckHere: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet weak var btnresend: UIButton!
    
    
    var dictData = NSMutableDictionary()
    var count = 30
    var timer = Timer()
    var strViewComeFrom = String()
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "OTP"

        
        tf1.layer.borderColor = UIColor.gray.cgColor
        tf1.layer.borderWidth = 1.0
        
        tf2.layer.borderColor = UIColor.gray.cgColor
        tf2.layer.borderWidth = 1.0
        
        tf3.layer.borderColor = UIColor.gray.cgColor
        tf3.layer.borderWidth = 1.0
        
        tf4.layer.borderColor = UIColor.gray.cgColor
        tf4.layer.borderWidth = 1.0
        
        lblShowCounter.text = "00" + ":" + "00"
        lblClckHere.isHidden = true
        btnresend.isHidden = true
        lblShowCounter.font = lblShowCounter.font.withSize(30)
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounterValue), userInfo: nil, repeats: true)
        
        var myMutableString1 = NSMutableAttributedString()
        let attrs1 = [NSAttributedString.Key.font : lbltitle.font, NSAttributedString.Key.foregroundColor : UIColor.gray]
        
        myMutableString1 = NSMutableAttributedString(string: lbltitle.text!, attributes:attrs1 as [NSAttributedString.Key : Any])
        myMutableString1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 10, length: 11))
        lbltitle.attributedText = myMutableString1
        btnSubmit.layer.borderColor = UIColor.darkGray.cgColor
        btnSubmit.layer.borderWidth = 1.0
    }
    // MARK: - --------------IBAction
    // MARK: -
  
    
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        if tf1.text?.count != 0 && tf2.text?.count != 0 && tf3.text?.count != 0 && tf4.text?.count != 0  {
                self.call_OTPVerify_API(sender: sender)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_OTP, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnResendSMS(_ sender: Any) {
            self.call_ResendOTP_API()
    }
    
    @objc func updateCounterValue() {
        if(count > 0){
            count = count - 1
            var minutes = String(count / 60)
            var seconds = String(count % 60)
            if count % 60 < 10{
                seconds = "0" + seconds
            }
            if count / 60 < 10{
                minutes = "0" + minutes
            }
            lblShowCounter.text = minutes + ":" + seconds
            lblClckHere.isHidden = true
            btnresend.isHidden = true
            lblShowCounter.font = lblShowCounter.font.withSize(30)
            
        }else{
            lblShowCounter.text = "If you did not received OTP then"
            lblClckHere.isHidden = false
            btnresend.isHidden = false
            lblShowCounter.font = lblShowCounter.font.withSize(15)
            timer.invalidate()
            var myMutableString2 = NSMutableAttributedString()
            let attrs2 = [NSAttributedString.Key.font : lblShowCounter.font, NSAttributedString.Key.foregroundColor : UIColor.gray]
            myMutableString2 = NSMutableAttributedString(string: lblShowCounter.text!, attributes:attrs2 as [NSAttributedString.Key : Any])
            myMutableString2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 24, length: 3))
            lblShowCounter.attributedText = myMutableString2
            
        }
        
    }
    
    
    
    
    // MARK: - ---------------API's Calling
    // MARK: -

    func call_OTPVerify_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let strOTP = tf1.text! + tf2.text! + tf3.text! + tf4.text!
          let urlForGetData = "\(API_UserVerificationByOTP)" + "username=\(dictData.value(forKey: "UserName")!)" + "&otp=\(strOTP)"
         //    let urlForGetData = "\(API_UserVerificationByOTP)" + "username=7974998700" + "&otp=\(strOTP)"
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

            WebService.callAPIBYGET(parameter: NSDictionary(), url: urlForGetData) { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  =  removeNullFromDict (dict : (responce.value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        FTIndicator.showNotification(withTitle: alertInfo, message: "OTP Verify successfully.")
                        if(self.strViewComeFrom == "Login"){
                            self.navigationController?.setNavigationBarHidden(true, animated: true)
                            let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                            
                        }else{
                            let dict = dict.value(forKey: "RegistrationDc")as! NSDictionary
                            if("\(dict.value(forKey: "Role")!)" == "Mentor"){
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: LoginMentor.self) {
                                        controller.view.tag = 99
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }else{
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: LoginAspirant.self) {
                                        controller.view.tag = 99
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func call_ResendOTP_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictDatasend = NSMutableDictionary()
          let urlForGetData = "\(API_ResendOTP)" + "MobileNumber=\(dictData.value(forKey: "UserName")!)"
        //    let urlForGetData = "\(API_ResendOTP)" + "MobileNumber=7974998700"

            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

            WebService.callAPIBYGET(parameter: dictDatasend, url: urlForGetData) { (responce, status) in
                FTIndicator.dismissProgress()
                
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.timer = Timer()
                        self.count = 30
                        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounterValue), userInfo: nil, repeats: true)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            }
        }
    }

}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension OTPVarifyVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
        }
        if textField == tf1 {
            tf1.text = string
            if tf1.text?.count == 1 {
                tf2.becomeFirstResponder()
            }
        }
        if textField == tf2 {
            tf2.text = string
            if tf2.text?.count == 1 {
                tf3.becomeFirstResponder()
            }
        }
        
        if textField == tf3 {
            tf3.text = string
            if tf3.text?.count == 1 {
                tf4.becomeFirstResponder()
            }
        }
 
        if textField == tf4 {
            tf4.text = string
            if tf4.text?.count == 1 {
                self.view.endEditing(true)
                if tf1.text?.count != 0 && tf2.text?.count != 0 && tf3.text?.count != 0 && tf4.text?.count != 0 {
                    // self.call_OTPVerify_API(sender: btnSubmit)
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_OTP, viewcontrol: self)
                }
            }
        }
        
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 0)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
}
