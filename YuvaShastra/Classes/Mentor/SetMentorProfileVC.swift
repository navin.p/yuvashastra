//
//  SetMentorProfileVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 3/28/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class SetMentorProfileVC: UIViewController {
  
    // MARK:
    // MARK: - Outlets
    @IBOutlet weak var txtullName: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    @IBOutlet weak var txtC_Password: ACFloatingTextfield!
    @IBOutlet weak var btnbtnC_PasswordShowHideLogin: UIButton!
    @IBOutlet weak var btnbtnPasswordShowHideLogin: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    
    
    var imagePicker = UIImagePickerController()
   var strProfileImageName = ""
    var dictRegisterData = NSMutableDictionary()
    // MARK:
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnNext.layer.borderColor = UIColor.darkGray.cgColor
        btnNext.layer.borderWidth = 1.0
        txtullName.text  = "\(dictRegisterData.value(forKey: "Name")!)"
        txtPassword.text = ""
        txtC_Password.text = ""
        self.navigationItem.title = "Set Profile"
        if("\(dictRegisterData.value(forKey: "Role")!)" == "Student"){
            imgProfile.image = UIImage(named: "aspirant_img")
        }else{
            imgProfile.image = UIImage(named: "mentor_img")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imgProfile.layer.borderWidth = 1.0
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        imgProfile.clipsToBounds = true
        imgProfile.layer.borderColor = UIColor.white.cgColor
      
        
    }
    // MARK:
    // MARK: - IBAction

    @IBAction func actionOnShowPassword(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if(sender.tag == 1){
            sender.currentBackgroundImage == (UIImage(named: "hidePassword")) ? (txtPassword.isSecureTextEntry = false) : (txtPassword.isSecureTextEntry = true)

        }else {
            sender.currentBackgroundImage == (UIImage(named: "hidePassword")) ? (txtC_Password.isSecureTextEntry = false) : (txtC_Password.isSecureTextEntry = true)
        }
        sender.currentBackgroundImage == (UIImage(named: "hidePassword")) ? sender.setBackgroundImage(UIImage(named: "showPassword"), for: .normal) : sender.setBackgroundImage(UIImage(named: "hidePassword"), for: .normal)
        
    }
    
    @IBAction func actionOnNext(_ sender: UIButton) {
        self.view.endEditing(true)

        if(validation()){
            
            self.dictRegisterData.setValue(txtullName.text!, forKey: "Name")
            self.dictRegisterData.setValue(txtPassword.text!, forKey: "password")
            self.dictRegisterData.setValue(strProfileImageName, forKey: "ProfileImage")

            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AreaofExpertiseVC")as! AreaofExpertiseVC
            testController.dictRegisterData = self.dictRegisterData
            testController.imgProfile = self.imgProfile.image!
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    
    @IBAction func actionOnEditProfileImage(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: Alert_SelectionMessage, message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            if(UIImagePickerController .isSourceTypeAvailable(.camera)){
                self.imagePicker.sourceType = .camera
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)
            } else {
            }
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: {
        })
    }
    // MARK: - --------------Validation
    // MARK: -
    func validation()-> Bool{
      
        if txtullName.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_FullName, viewcontrol: self)

            return false
        }   else if ( (txtPassword.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Password, viewcontrol: self)

            return false
        }
        else if ((txtPassword.text?.count)! < 6){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PasswordLimit, viewcontrol: self)

            return false
        }else if ((txtC_Password.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CPassword, viewcontrol: self)
            
            return false
        }
        else if (txtC_Password.text != txtPassword.text!){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CPasswordCheck, viewcontrol: self)
            return false
        }
        return true
    }
}
// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -

extension SetMentorProfileVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            self.imgProfile.contentMode = .scaleAspectFill
            self.imgProfile.image = image
            self.strProfileImageName = getUniqueString()
            self.dismiss(animated: false) {
            }
        } else {
            self.dismiss(animated: false) {
            }
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  false, completion: nil)
    }
}
//MARK:-
//MARK:- ---------TextField Extension

extension SetMentorProfileVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (  textField == txtullName )
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "All", limitValue: 85)
        }
     
        else if textField == txtPassword || textField == txtC_Password
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "All", limitValue: 11)
        }
        else
        {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
