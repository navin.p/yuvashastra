//
//  MyAnswerDetailVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/2/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class MyAnswerDetailVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    var aryList = NSMutableArray()
    var dictAnswerData = NSMutableDictionary()
    // MARK: - ----------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "My Answer"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        aryList = NSMutableArray()
        aryList = (dictAnswerData.value(forKey: "userAnswerReportDc")as! NSArray).mutableCopy()as! NSMutableArray
        self.tvlist.reloadData()
        
    }
    

    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MyAnswerDetailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "MyAnswerDetailCell", for: indexPath as IndexPath) as! MyAnswerDetailCell
               let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            cell.lblAns.text = "Ans. \(dict.value(forKey: "Answer")!)"
            cell.lblAnsPostedby.text = "Posted By : \(dict.value(forKey: "PostedBy")!)\nPosted on : \(dateStringToFormatedDateString(dateToConvert: dict.value(forKey: "CreatedDate") as! String, dateFormat: "dd/MM/yyy"))"
        if(indexPath.row ==  0){
            cell.lblQuestion.text = "Q. \(dictAnswerData.value(forKey: "Question")!)"
            cell.lblPostedBy.text = "Posted By : \(dictAnswerData.value(forKey: "PostedBy")!)\nPosted on : \(dateStringToFormatedDateString(dateToConvert: dictAnswerData.value(forKey: "CreatedDate") as! String, dateFormat: "dd/MM/yyy"))"
        }else{
            cell.lblQuestion.text = ""
            cell.lblPostedBy.text = ""
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  
    
}

// MARK: - ----------------ChatCell
// MARK: -
class MyAnswerDetailCell: UITableViewCell {
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblPostedBy: UILabel!
    @IBOutlet weak var lblAns: UILabel!
    @IBOutlet weak var lblAnsPostedby: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
