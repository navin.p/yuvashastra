//
//  AddQuestionVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/5/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit
import CoreData

class AddQuestionVC: UIViewController {

    @IBOutlet weak var lblSelectCategory: UILabel!
    @IBOutlet weak var lblSelectSubCategory: UILabel!
    @IBOutlet weak var lblSelectSubject: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtView: KMPlaceholderTextView!
    var ary_for_Category = NSMutableArray()
    var ary_for_SubCategory = NSMutableArray()
    var ary_for_Subject = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.layer.borderColor = UIColor.darkGray.cgColor
        btnNext.layer.borderWidth = 1.0
        self.navigationItem.title = "Add Question"
        self.navigationController?.navigationBar.prefersLargeTitles = false
         self.lblSelectCategory.tag = 0
        self.lblSelectSubCategory.tag = 0
        self.lblSelectSubject.tag = 0

    }

    @IBAction func actionOnSelectCategory(_ sender: UIButton) {
        if(dataFromLocal(strEntity: "AreaofExpertise", strkey: "areaofExpertise").count != 0){
            self.ary_for_Category = NSMutableArray()
            self.ary_for_Category = (dataFromLocal(strEntity: "AreaofExpertise", strkey: "areaofExpertise")as NSArray).mutableCopy()as! NSMutableArray
            OpenPopListView(strTag: 41, aryTemp: self.ary_for_Category)
        }else{
            self.call_GetCategoryDropdown_API()
        }
    }
    @IBAction func actionOnSelectSubCategory(_ sender: UIButton) {
        if lblSelectCategory.tag == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select category first.", viewcontrol: self)
        }else{
            call_GetSubCategoryDropdown_API(strCategoryId: "\(lblSelectCategory.tag)")
        }
    }
    @IBAction func actionOnSelectSubject(_ sender: UIButton) {
        if lblSelectSubCategory.tag == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select sub category first.", viewcontrol: self)
        }else{
            call_GetSubjectDropdown_API(strSubCategoryId: "\(lblSelectSubCategory.tag)")
        }
    }
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        if lblSelectCategory.tag == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please Select Category", viewcontrol: self)
        }else if (txtView.text.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Question required!", viewcontrol: self)
        }
        else{
                self.AddQuestionApiCalling(sender: sender)
        }
    }
    
    // MARK: - --------------Extra Function
    // MARK: -
    func OpenPopListView(strTag : Int , aryTemp : NSMutableArray) {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "---Select---"
        vc.strTag = strTag
        if aryTemp.count != 0{
            let dict = NSMutableDictionary()
            dict.setValue("All", forKey: "CategoryName")
            dict.setValue("0", forKey: "CategoryId")
            if(strTag == 41){
                if !(aryTemp.contains(dict)){
                    aryTemp.insert(dict, at: 0)
                }
            }
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.aryTBL = aryTemp
            vc.delegate = self
            self.present(vc, animated: false, completion: {})
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "", viewcontrol: self)
        }
        
    }
    
    // MARK: - --------------Local Data Base
    // MARK: -
    func dataFromLocal(strEntity: String ,strkey : String )-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: strEntity, strkey: strkey)
        
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: strkey) ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }


    // MARK:
    // MARK: - API Calling
    func AddQuestionApiCalling(sender : UIButton){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            let Question = "\(self.txtView.text!)"
            let CategoryId = "\(self.lblSelectCategory.tag)"
            let SubCategoryId = "\(self.lblSelectSubCategory.tag)"
            let NestedSubCategoryId = "\(self.lblSelectSubject.tag)"
            let PostedByUserId = "\(dict_Login_Data.value(forKey: "UserId")!)"
            
            let urlForGetData = "\(API_AddQuestion)" + "Question=\(Question)" + "&CategoryId=\(CategoryId)" + "&SubCategoryId=\(SubCategoryId)" + "&NestedSubCategoryId=\(NestedSubCategoryId)" + "&PostedByUserId=\(PostedByUserId)"
            
            
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: urlForGetData) { (responce, status) in
                FTIndicator.dismissProgress()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        FTIndicator.showToastMessage("\(dict.value(forKey: "Success")as! String)")
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_GetCategoryDropdown_API( )  {
        if !(isInternetAvailable()){
        }else{
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: API_GetCategoryDropdown) { (responce, status) in
                
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "categoryDropdownDc")as! NSArray)
                        self.ary_for_Category = NSMutableArray()
                        self.ary_for_Category = aryData.mutableCopy()as! NSMutableArray
                        deleteAllRecords(strEntity:"AreaofExpertise")
                        saveDataInLocalArray(strEntity: "AreaofExpertise", strKey: "areaofExpertise", data: self.ary_for_Category)
                        self.OpenPopListView(strTag: 41, aryTemp: self.ary_for_Category)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)

                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            }
        }
    }
    func call_GetSubCategoryDropdown_API(strCategoryId : String )  {
        if !(isInternetAvailable()){
        }else{
          let urlCall =  API_GetSubCategoryDropdown + "CategoryId=" + strCategoryId
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: urlCall) { (responce, status) in
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "subCategoryDropdownDc")as! NSArray)
                        self.ary_for_SubCategory = NSMutableArray()
                        self.ary_for_SubCategory = aryData.mutableCopy()as! NSMutableArray
                        self.OpenPopListView(strTag: 42, aryTemp: self.ary_for_SubCategory)

                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)

                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            }
        }
    }
    func call_GetSubjectDropdown_API(strSubCategoryId : String )  {
        if !(isInternetAvailable()){
        }else{
            let urlCall =  API_GetNestedSubCategoryDropdown + "SubCategoryId=" + strSubCategoryId
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: urlCall) { (responce, status) in
                
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "nestedSubCategoryDropdownDc")as! NSArray)
                        self.ary_for_Subject = NSMutableArray()
                        self.ary_for_Subject = aryData.mutableCopy()as! NSMutableArray
                        self.OpenPopListView(strTag: 43, aryTemp: self.ary_for_Subject)

                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)

                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            }
        }
    }
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension AddQuestionVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 41){
            self.lblSelectCategory.textColor = UIColor.black
            self.lblSelectSubCategory.textColor = UIColor.lightGray
            self.lblSelectSubject.textColor = UIColor.lightGray

            self.lblSelectCategory.text = "\(dictData.value(forKey: "CategoryName")!)"
            self.lblSelectCategory.tag = Int("\(dictData.value(forKey: "CategoryId")!)")!
            self.lblSelectSubCategory.text = "Select Sub Category"
            self.lblSelectSubCategory.tag = Int("0")!
            self.lblSelectSubject.text = "Select Subject"
            self.lblSelectSubject.tag = Int("0")!
        }else if(tag == 42){
                self.lblSelectSubCategory.textColor = UIColor.black
                  self.lblSelectSubject.textColor = UIColor.lightGray
                self.lblSelectSubCategory.text = "\(dictData.value(forKey: "SubCategoryName")!)"
                self.lblSelectSubCategory.tag = Int("\(dictData.value(forKey: "SubCategoryId")!)")!
                self.lblSelectSubject.text = "Select Subject"
               self.lblSelectSubject.tag = Int("0")!
        }else  if(tag == 43){
            self.lblSelectSubject.textColor = UIColor.black
            self.lblSelectSubject.text = "\(dictData.value(forKey: "NestedSubCategoryName")!)"
            self.lblSelectSubject.tag = Int("\(dictData.value(forKey: "NestedSubCategoryId")!)")!
        }
    }
}
// MARK:
// MARK: - UITextViewDelegate
extension AddQuestionVC : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return txtViewValidation(textField: textView, string: text, returnOnly: "ALL", limitValue: 500)
    }
}
