//
//  WebViewVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/25/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController {
    
    // MARK: - ---------------IBOutlet
    // MARK: -
    
    @IBOutlet weak var viewWeb: UIWebView!
    
    var strViewComeFrom = ""
    var webURL = ""
    
    // MARK: - ---------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationItem.title = strViewComeFrom
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !(isInternetAvailable()){
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            viewWeb.loadRequest(URLRequest(url: URL(string: webURL)!))
            viewWeb.scrollView.maximumZoomScale = 8
            viewWeb.scrollView.minimumZoomScale = 1
            viewWeb.delegate = self
        }
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self, viewMaxY: Int((self.navigationController?.navigationBar.bounds.maxY)!))
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
    
}
// MARK :
//MARK : UIWebViewDelegate
extension WebViewVC : UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
      //  dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
        FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
     //   remove_dotLoader(controller: self)
        FTIndicator.dismissProgress()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
      //  remove_dotLoader(controller: self)
        FTIndicator.dismissProgress()

    }
}
