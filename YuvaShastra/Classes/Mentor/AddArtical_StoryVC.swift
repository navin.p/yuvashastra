//
//  AddArtical_StoryVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/2/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class AddArtical_StoryVC: UIViewController {
   
    var strComeFrom = String()
    // MARK:
    // MARK: - IBOutlet
    @IBOutlet weak var txtTitle: ACFloatingTextfield!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtview: KMPlaceholderTextView!
    // MARK:
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = strComeFrom
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        btnNext.layer.borderColor = UIColor.lightGray.cgColor
        btnNext.layer.borderWidth = 1.0
        txtTitle.text = ""
        txtview.text = ""
    }
    // MARK:
    // MARK: - IBAction
    @IBAction func actionOnSubmitBottom(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtTitle.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Title required!", viewcontrol: self)
        }
        else if txtview.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Description required!", viewcontrol: self)
        }
        else{
            self.AddArticle_StoryCalling(sender: sender)
        }
    }
    // MARK:
    // MARK: - API Calling
    func AddArticle_StoryCalling(sender : UIButton){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            
            var urlForGetData = ""
            
            if(self.strComeFrom == "Add Article"){
                urlForGetData = "\(API_AddArticle)" + "Title=\(self.txtTitle.text!)" + "&Description=\(self.txtview.text!)" + "&PostedByUserId=\(dict_Login_Data.value(forKey: "UserId")!)" + "&city_id=\(dict_Login_Data.value(forKey: "city_id")!)"

            }else{
                 urlForGetData = "\(API_AddInspirationalStories)" + "Title=\(self.txtTitle.text!)" + "&Description=\(self.txtview.text!)" + "&PostedByUserId=\(dict_Login_Data.value(forKey: "UserId")!)" + "&city_id=\(dict_Login_Data.value(forKey: "city_id")!)"
            }

            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: urlForGetData) { (responce, status) in
                FTIndicator.dismissProgress()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        FTIndicator.showToastMessage("\(dict.value(forKey: "Success")as! String)")
                       self.navigationController?.popViewController(animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
 
}
// MARK:
// MARK: - UITextFieldDelegate
extension AddArtical_StoryVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (  textField == txtTitle)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "All", limitValue: 85)
        }
      
        else
        {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
// MARK:
// MARK: - UITextViewDelegate
extension AddArtical_StoryVC : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return txtViewValidation(textField: textView, string: text, returnOnly: "ALL", limitValue: 500)
    }
}
