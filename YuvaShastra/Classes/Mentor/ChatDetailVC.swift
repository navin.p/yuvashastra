//
//  ChatDetailVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/1/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class ChatDetailVC: UIViewController {
    var dictData = NSMutableDictionary()
    var aryList = NSMutableArray()
    var refresher = UIRefreshControl()
    var userID = String()
    var strErrorMessage = String()
    var intNumberofRow = 0

    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var textField: KMPlaceholderTextView!


    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "\(dictData.value(forKey: "Name")!)"
       self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.refresher.tintColor = hexStringToUIColor(hex: primaryBlueColor)
        self.tvlist!.addSubview(refresher)
         self.tvlist.scrollToBottom(animated: true)
        userID = "\(dict_Login_Data.value(forKey: "UserId")!)"
        
        self.call_GetChatData_API(tag: 1)
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        self.call_GetChatData_API(tag: 2)
    }
    @IBAction func sendText(_ sender: UIButton) {
        if(textField.text.count != 0){
            AddChatApiCalling(sender: sender)
        }
    }
    
    // MARK: - ---------------API's Calling
    // MARK: -
 
    
    func AddChatApiCalling(sender : UIButton){
        if !(isInternetAvailable()){
          
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            let dictChatData = NSMutableDictionary()
            dictChatData.setValue("0", forKey: "MSChatId")
            dictChatData.setValue("\(dictData.value(forKey: "UserId")!)", forKey: "PostedToUserId")
            dictChatData.setValue(userID, forKey: "PostedByUserId")
            dictChatData.setValue(textField.text + " ", forKey: "Message")
            WebService.postRequestWithHeaders(dictJson: dictChatData, url: API_AddChat) { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.textField.text = ""
                        self.call_GetChatData_API(tag: 1)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_GetChatData_API( tag : Int)  {
        if !(isInternetAvailable()){
            self.strErrorMessage =  alertInternet
            intNumberofRow = 1
            self.tvlist.reloadData()
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            if tag == 1{
                loading.startLoading(text: "Loading Chat...")
            }
            let dictDatasend = NSMutableDictionary()

            let urlForGetData = "\(API_GetChatList)" + "PostedByUserId=\(userID)"  + "&PostedToUserId=\(dictData.value(forKey: "UserId")!)"
            
            WebService.callAPIBYGET(parameter: dictDatasend, url: urlForGetData ) { (responce, status) in
                if tag == 1{
                    loading.endLoading()
                }else{
                    self.refresher.endRefreshing()
                }
                removeErrorView()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "ChatBoardDc")as! NSArray)
                        self.aryList = NSMutableArray()
                        self.aryList = aryData.mutableCopy()as! NSMutableArray
                        self.tvlist.scrollToBottom(animated: true)
                    }else{
                        self.strErrorMessage =  "\(dict.value(forKey: "Success")as! String)"
                    }
                }else{
                    self.strErrorMessage =  alertDataNotFound
                }
                self.intNumberofRow = 1
                self.tvlist.reloadData()
            }
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ChatDetailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count != 0 ? aryList.count : intNumberofRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(aryList.count != 0){
            let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            let msg =  "\(dict.value(forKey: "Message")!)"
            let date =  "\(dict.value(forKey: "PostedDate")!)"
            
            if("\(dict.value(forKey: "PostedByUserId")!)" == userID){ // Self
                
                let cell = tvlist.dequeueReusableCell(withIdentifier: "SenderCell", for: indexPath as IndexPath) as! ChatDetailCell
                cell.lblMessage.text = "\n" + "  " + msg  + "   " + "\n"
                cell.lblMessage.layer.cornerRadius = 10.0
                cell.lblMessage.layer.masksToBounds = true
                cell.lblDate.text = dateStringToFormatedDateString(dateToConvert: date, dateFormat: "dd/MM/yyy")
                return cell
                
            }else{ // OTHER
                let cell = tvlist.dequeueReusableCell(withIdentifier: "RecieverCell", for: indexPath as IndexPath) as! ChatDetailCell
                cell.lblMessage.text =  "\n" + "  " + msg  + "   " + "\n"
                cell.lblDate.text = dateStringToFormatedDateString(dateToConvert: date, dateFormat: "dd/MM/yyy")
                
                cell.lblMessage.layer.cornerRadius = 10.0
                cell.lblMessage.layer.masksToBounds = true
                
                return cell
            }
            
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ChatCellError", for: indexPath as IndexPath) as! ChatCell
            cell.lblErrorTitle.text = strErrorMessage
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return aryList.count != 0 ? UITableView.automaticDimension : self.tvlist.frame.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if( aryList.count == 0){
            intNumberofRow = 0
            self.tvlist.reloadData()
            self.call_GetChatData_API(tag: 1)
        }
    }
 
    }

// MARK: - ----------------ChatCell
// MARK: -
class ChatDetailCell: UITableViewCell {
  
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ----------------UITextViewDelegate
// MARK: -
extension ChatDetailVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return txtViewValidation(textField: textView, string: text, returnOnly: "All", limitValue: 99999)

    }
    
    
}

extension UITableView {
    func scrollToBottom(animated: Bool = true) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if (rows > 0){
            self.scrollToRow(at: NSIndexPath(row: rows - 1, section: sections - 1) as IndexPath, at: .bottom, animated: true)
        }
    }
}
