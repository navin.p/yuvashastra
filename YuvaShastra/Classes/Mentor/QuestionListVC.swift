
//
//  QuestionListVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/2/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit
class QuestionListVC: UIViewController {
    
      @IBOutlet weak var tvlist: UITableView!
      @IBOutlet weak var searchBar: UISearchBar!
  
    var refresher = UIRefreshControl()
    var aryList = NSMutableArray()
    var aryForListData = NSMutableArray()
    var strErrorMessage = String()
    var intNumberofRow = 0
    // MARK: - --------------Life Cycle
    // MARK: -
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let logo = UIImage(named: "toolbar_logo")
//        let imageView = UIImageView(image:logo)
//        imageView.contentMode = .scaleAspectFit
//        self.navigationItem.titleView = imageView
        let BarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(More))
        BarButtonItem.image = UIImage(named: "home_icon")
        self.navigationItem.title = "Questions"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(Add))
    //    navigationItem.rightBarButtonItems = [add, BarButtonItem]
        
        if(dict_Login_Data.value(forKey: "Role")as! String == "Mentor"){
         //   navigationItem.rightBarButtonItems = [BarButtonItem]
        }else{
            navigationItem.rightBarButtonItems = [add]
        }
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.refresher.tintColor = hexStringToUIColor(hex: primaryBlueColor)
        self.tvlist!.addSubview(refresher)
        self.tvlist.reloadData()
        searchBar.backgroundColor = UIColor.groupTableViewBackground
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.call_GetListData_API(tag: 1)

    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        self.call_GetListData_API(tag: 2)
    }
    
    // MARK: - ------------More
    // MARK: -
    @objc func More(){
        self.view.endEditing(true)
    }
    @objc func Add(){
        self.view.endEditing(true)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddQuestionVC")as! AddQuestionVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_GetListData_API( tag : Int)  {
        if !(isInternetAvailable()){
            strErrorMessage = alertInternet
            intNumberofRow = 1
            self.tvlist.reloadData()
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            //----1
            if tag == 1{
                loading.startLoading(text: "Loading...")
            }
            //---2
            
            let aryList = (dict_Login_Data.value(forKey: "userCategoryMappingDc")as! NSArray).mutableCopy()as! NSMutableArray
            var strId = ""
            for item in aryList{
                strId = strId + "\((item as AnyObject).value(forKey: "CategoryId")!),"
            }
            if(strId.count != 0){
                strId = String(strId.dropLast())
            }
            
            let dictData = NSMutableDictionary()
            dictData.setValue("0", forKey: "Skip")
            dictData.setValue("10", forKey: "Take")
            dictData.setValue(strId, forKey: "CategoryIds")
            dictData.setValue("", forKey: "CategoryName")
            dictData.setValue("", forKey: "Question")
            dictData.setValue("", forKey: "PostedBy")
            
            let urlForGetData = "\(API_GetQuestion)" + "QuestionSearchDc="
            WebService.postRequestWithHeaders(dictJson: dictData, url: urlForGetData) { (responce, status) in
                print(responce)
                if tag == 1{
                    loading.endLoading()
                }else{
                    self.refresher.endRefreshing()
                }
                removeErrorView()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "QuestionTableDc")as! NSArray)
                        let aryFinalData = NSMutableArray()
                        for item in aryData{
                            var dictQuestion = NSMutableDictionary()
                            dictQuestion = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            let aryLike = (dictQuestion.value(forKey: "questionLikeCommentDc")as! NSArray).mutableCopy()as! NSMutableArray
                            let aryLikeCommentTemp = NSMutableArray()
                            for item in aryLike{
                                var dictLike = NSMutableDictionary()
                                dictLike = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                let strCommentedID = "\(dictLike.value(forKey: "UserId")!)"
                                let strType = "\(dictLike.value(forKey: "Type")!)"

                                if(("\(dict_Login_Data.value(forKey: "UserId")!)" == strCommentedID)  && (strType == "Like")){
                                    aryLikeCommentTemp.add(dictLike)
                                }
                            }
                           dictQuestion.setValue(aryLikeCommentTemp, forKey: "questionLikeCommentDc")
                            aryFinalData.add(dictQuestion)
                        }
                        self.aryList = NSMutableArray()
                        self.aryList = aryFinalData.mutableCopy()as! NSMutableArray
                        self.aryForListData = NSMutableArray()
                        self.aryForListData = aryFinalData.mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                    }else{
                        self.strErrorMessage =  "\(dict.value(forKey: "Success")as! String)"
                    }
                }else{
                    self.strErrorMessage =  alertSomeError
                }
                self.intNumberofRow = 1
                self.tvlist.reloadData()
            }
        }
    }

    func QuestionLikeApiCalling(sender : UIButton){
        let dict = removeNullFromDict(dict: (aryList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
     
            let urlForGetData = "\(API_AddQuestionLikesComments)" + "QuestionId=\(dict.value(forKey: "QuestionId")!)" + "&UserId=\(dict_Login_Data.value(forKey: "UserId")!)" + "&Type=Like" + "&Comment="
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: urlForGetData) { (responce, status) in
                FTIndicator.dismissProgress()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let dictlike = NSMutableDictionary()
                        if(sender.currentImage != UIImage(named: "like_icon_2")){
                            dictlike.setValue("\(dict_Login_Data.value(forKey: "UserId")!)", forKey: "UserId")
                            dictlike.setValue("Like", forKey: "Type")
                        }else{
                            dictlike.setValue("", forKey: "UserId")
                            dictlike.setValue("Like", forKey: "Type")
                        }
                        let dictForIndex = (self.aryList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        let aryTem = NSMutableArray()
                        aryTem.add(dictlike)
                        dictForIndex.setValue(aryTem, forKey: "questionLikeCommentDc")
                        self.aryList.replaceObject(at: sender.tag, with: dictForIndex)
                        self.aryForListData.replaceObject(at: sender.tag, with: dictForIndex)
                       self.tvlist.reloadData()
                       
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
  
    

}
// MARK: - -------------UISearchBarDelegate
// MARK: -
extension  QuestionListVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
     self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
       self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {

        let resultPredicate = NSPredicate(format: "Question contains[c] %@ OR PostedBy contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryForListData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryForListData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        if(aryList.count == 0){
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension QuestionListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count == 0 ? intNumberofRow : aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(aryList.count != 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "HomeCommentCell", for: indexPath as IndexPath) as! HomeCommentCell
            let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            cell.lblCategoryName.text = "\(dict.value(forKey: "CategoryName")!)"
            cell.lblPostedby.text = "Posted by : \(dict.value(forKey: "PostedBy")!)\n" + "Posted on :" + dateStringToFormatedDateString(dateToConvert: "\(dict.value(forKey: "CreatedDate")!)", dateFormat: "dd/MM/yyy hh:mm a")
            cell.lblQuestion.text = "Q. \(dict.value(forKey: "Question")!)"
            let aryAns = dict.value(forKey: "answerMobileDc")as! NSArray
            
            if(aryAns.count != 0){
                let dict = removeNullFromDict(dict: (aryAns.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                cell.lblAns.text = "Ans. \(dict.value(forKey: "Answer")!)"
                cell.lblAnsDate.text = dateStringToFormatedDateString(dateToConvert: "\(dict.value(forKey: "CreatedDate")!)", dateFormat: "dd/MM/yyy hh:mm a")
            }else{
                cell.lblAns.text = ""
                cell.lblAnsDate.text = ""
            }
            cell.lblPostedby.textColor = hexStringToUIColor(hex: primaryBlueColor)
            cell.lblAnsDate.textColor = hexStringToUIColor(hex: primaryBlueColor)
            cell.lblCategoryName.textColor = hexStringToUIColor(hex: primaryBlueColor)
            cell.btnaddAnswer.setTitleColor(hexStringToUIColor(hex: primaryBlueColor), for: .normal)
            if(dict_Login_Data.value(forKey: "Role")as! String != "Mentor"){
                cell.btnaddAnswer.setTitle("", for: .normal)
            }else{
                cell.btnaddAnswer.setTitle("Add answer", for: .normal)
                cell.btnaddAnswer.addTarget(self, action: #selector(buttonAddAnswer), for: .touchUpInside)
                cell.btnaddAnswer.tag = indexPath.row
            }
            
            cell.imgView.layer.borderWidth = 1.0
            cell.imgView.layer.masksToBounds = false
            cell.imgView.layer.cornerRadius = 40.0
            cell.imgView.clipsToBounds = true
            cell.imgView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            let urlImage = "\(BaseURLImageDownload)\(dict.value(forKey: "Question")!)"
            cell.imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "aspirant_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                //print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            cell.btnShare.addTarget(self, action: #selector(buttonShare), for: .touchUpInside)
            cell.btnShare.tag = indexPath.row
            cell.btnComment.addTarget(self, action: #selector(buttonComment), for: .touchUpInside)
            cell.btnComment.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(buttonLike), for: .touchUpInside)
            cell.btnLike.tag = indexPath.row
            cell.btnLike.setImage(UIImage(named: "like_icon_1"), for: .normal)
            let aryLike = dict.value(forKey: "questionLikeCommentDc")as! NSArray
            for item in aryLike{
                let dict  =  (item as AnyObject)as! NSDictionary
                let strID = "\(dict.value(forKey: "UserId")!)"
                if(strID == "\(dict_Login_Data.value(forKey: "UserId")!)"){
                    cell.btnLike.setImage(UIImage(named: "like_icon_2"), for: .normal)
                    break
                }else{
                    cell.btnLike.setImage(UIImage(named: "like_icon_1"), for: .normal)
                }
            }
            
            return cell
        }
        else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ChatCellError", for: indexPath as IndexPath) as! ChatCell
            cell.lblErrorTitle.text = strErrorMessage
            return cell
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return aryList.count != 0 ? UITableView.automaticDimension : self.tvlist.frame.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count == 0){
            intNumberofRow = 0
            tvlist.reloadData()
            self.call_GetListData_API(tag: 1)
        }else{
        let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        if( (dict.value(forKey: "answerMobileDc")as! NSArray).mutableCopy()as! NSMutableArray).count != 0{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "QuestionListDetailVC")as! QuestionListDetailVC
            testController.dictQuestionAnswerData = dict
            self.navigationController?.pushViewController(testController, animated: true)
        }else{
         //   FTIndicator.showToastMessage("Answer ")
        }
        }
    }
    
  
    
    @objc func buttonShare(sender: UIButton!) {
        let dict = removeNullFromDict(dict: (aryList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            let activityViewController = UIActivityViewController(activityItems: ["Q. \(dict.value(forKey: "Question")!)"], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func buttonLike(sender: UIButton!) {
       self.QuestionLikeApiCalling(sender: sender)
        
    }
    @objc func buttonComment(sender: UIButton!) {
        let dict = removeNullFromDict(dict: (aryList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "CommentVC")as! CommentVC
        testController.dictData = dict
        testController.strComeFromView = "Question"
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @objc func buttonAddAnswer(sender: UIButton!) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddAnswerVC")as! AddAnswerVC
        let dict = removeNullFromDict(dict: (aryList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        testController.dictQuestion = dict
        self.navigationController?.pushViewController(testController, animated: true)
    }
}

// MARK: - ----------------ChatCell
// MARK: -
class HomeCommentCell: UITableViewCell {
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblPostedby: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblAns: UILabel!
    @IBOutlet weak var lblAnsDate: UILabel!
    @IBOutlet weak var btnaddAnswer: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
