//
//  MentorProfile.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 3/28/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class MentorProfile: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    var aryList = NSMutableArray()

    @IBOutlet weak var txtQualification: ACFloatingTextfield!
    @IBOutlet weak var txtPosition: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtName: ACFloatingTextfield!
    @IBOutlet weak var imgView: UIImageView!

    @IBOutlet var viewChangePassword: UIView!
    @IBOutlet weak var txtOldPassword: ACFloatingTextfield!
    @IBOutlet weak var txtNewPassword: ACFloatingTextfield!
    @IBOutlet weak var txtConfirmPassword: ACFloatingTextfield!
    @IBOutlet weak var imgViewBackground: UIImageView!
    var blurEffect = UIBlurEffect(style: .prominent )
    var blurEffectView = UIVisualEffectView()
    // MARK: - --------------Life cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDataOnView()
         blurEffect = UIBlurEffect(style: .prominent )
         blurEffectView = UIVisualEffectView(effect: blurEffect)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        aryList = [["title":"My Answer","image":"home"],["title":"My Area of Expertise","image":"add_category"],["title":"My Article","image":"add_category"],["title":"News","image":"add_category"],["title":"Inspirational Stories","image":"add_user"],["title":"Add Article","image":"add_sub_category"],["title":"Add Story","image":"disclaimer"],["title":"Reset Password","image":"developer_info"]]
     
    }
    override func viewWillLayoutSubviews() {
     
        blurEffectView.frame = self.imgViewBackground.frame
        self.imgViewBackground.willRemoveSubview(blurEffectView)

        self.imgViewBackground.insertSubview(blurEffectView, at: 0)
    }
    
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBAction func actionOnCross(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            self.viewChangePassword.frame = CGRect(x:0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.viewChangePassword.removeFromSuperview()
        }
    }
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        if(validation()){
            self.ForgotPasswordCalling(sender: sender)
        }
    }
    

    // MARK: - --------------setDataOnView
    // MARK: -
    func setDataOnView()  {
        txtName.text =  "\(dict_Login_Data.value(forKey: "Name")!)"
        txtMobile.text =  "\(dict_Login_Data.value(forKey: "MobileNumber")!)"
        txtPosition.text =  "\(dict_Login_Data.value(forKey: "CurruntPosition")!)"
        let aryListTemp = (dict_Login_Data.value(forKey: "userQualificationMappingDc")as! NSArray).mutableCopy()as! NSMutableArray
        if(aryListTemp.count != 0){
            txtQualification.text =  "\(((aryListTemp.object(at: 0)as! NSDictionary).value(forKey: "Qualification")!))"
        }
        let imageName  =  "\(dict_Login_Data.value(forKey: "ProfileImage")!)"
        imgView.layer.borderWidth = 1.0
        imgView.layer.masksToBounds = false
        imgView.layer.cornerRadius = imgView.frame.height/2
        imgView.clipsToBounds = true
        imgView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        let urlImage = "\(BaseURLImageDownload)" + imageName
        self.imgViewBackground.image = self.imgView.image
        var strImagePlaceholder = "mentor_img"
        if(dict_Login_Data.value(forKey: "Role")as! String == "Student"){
            strImagePlaceholder = "aspirant_img"
        }
        
        imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: strImagePlaceholder), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            //print(url ?? 0)
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
            self.imgView.isUserInteractionEnabled = true
           self.imgView.addGestureRecognizer(tapGestureRecognizer)
            self.imgViewBackground.image = self.imgView.image
       
        }, usingActivityIndicatorStyle: .gray)
    }
    @objc func connected(_ sender:AnyObject){
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
        testController.img = self.imgView.image!
        self.navigationController?.pushViewController(testController, animated: true)
    }
    // MARK: - --------------API Calling
    // MARK: -
    func ForgotPasswordCalling(sender : UIButton){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            let API_ForgetPassword :String =  API_ChangePassword + "UserName=\(dict_Login_Data.value(forKey: "UserName")!)&OldPassword=\(txtOldPassword.text!)&NewPassword=\(txtNewPassword.text!)"
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: API_ForgetPassword) { (responce, status) in
                FTIndicator.dismissProgress()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        FTIndicator.showToastMessage("\(dict.value(forKey: "Success")as! String)")
                        UIView.animate(withDuration: 0.5) {
                            self.viewChangePassword.frame = CGRect(x:0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                            self.viewChangePassword.removeFromSuperview()
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    // MARK: - ---------------validation
    // MARK: -
    func validation() -> Bool {
        if(txtOldPassword.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_OldPassword, viewcontrol: self)
            return false
        }
        else if (txtNewPassword.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertPassword, viewcontrol: self)
            return false
        }else if (txtConfirmPassword.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCPassword, viewcontrol: self)
            return false
        }else if (txtNewPassword.text! != txtConfirmPassword.text!){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CPasswordmatch, viewcontrol: self)
            return false
        }
        return true
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MentorProfile: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
               return aryList.count
        }else{
               return 1
        }
     
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath as IndexPath) as! ProfileCell
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
             cell.lblTitle.text = dict["title"]as? String
            return cell
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ProfileCell1", for: indexPath as IndexPath) as! ProfileCell1
            cell.btnLogout.setTitle("LOGOUT", for: UIControl.State.normal)
            cell.btnLogout.addTarget(self, action: #selector(logout), for: .touchUpInside)
            cell.btnVersion.setTitle("Version : \(app_Version)", for: UIControl.State.normal)
            cell.btnVersion.addTarget(self, action: #selector(Version), for: .touchUpInside)
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.section == 0){
            return 50
        }else{
            return 90
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row == 7 ){
            self.view.endEditing(true)
            viewChangePassword.frame = CGRect(x: 0, y: -self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            UIView.animate(withDuration: 0.5) {
                self.viewChangePassword.frame = CGRect(x:0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }
            self.view.addSubview(self.viewChangePassword)
        }
            
            
            
        else if (indexPath.row == 5 ){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddArtical_StoryVC")as! AddArtical_StoryVC
           testController.strComeFrom = "Add Article"
            self.navigationController?.pushViewController(testController, animated: true)
        }
        else if (indexPath.row == 6 ){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddArtical_StoryVC")as! AddArtical_StoryVC
            testController.strComeFrom = "Add Story"
            self.navigationController?.pushViewController(testController, animated: true)
        }
        else if (indexPath.row == 3 ){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "NewsVC")as! NewsVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        else if (indexPath.row == 2 ){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ArticleVC")as! ArticleVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        else if (indexPath.row == 4 ){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "InspirationalStoriesVC")as! InspirationalStoriesVC
            self.navigationController?.pushViewController(testController, animated: true)
        }else if (indexPath.row == 1 ){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyExpertiseVC")as! MyExpertiseVC
            self.navigationController?.pushViewController(testController, animated: true)
        }else if (indexPath.row == 0 ){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyAnswerVC")as! MyAnswerVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    
  
    
    @objc func logout() {
        let alert = UIAlertController(title: alertMessage, message: alertLogout, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
            print("Yay! You brought your towel!")
        }))
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            deleteAllRecords(strEntity:"LoginData")
            deleteAllRecords(strEntity:"AreaofExpertise")
            let obj_ISVC : WelcomeScreenVC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenVC") as! WelcomeScreenVC
            let navigationController = UINavigationController(rootViewController: obj_ISVC)
            navigationController.setNavigationBarHidden(false, animated: true)
           appDelegate.rootViewController(nav: navigationController)
            
        
        }))
        
        self.present(alert, animated: true)
    }
      @objc func Version() {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage:app_VersionInfo , viewcontrol: self)
    }
}

// MARK: - ----------------ChatCell
// MARK: -
class ProfileCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
}
    class ProfileCell1: UITableViewCell {
        @IBOutlet weak var btnLogout: UIButton!
        @IBOutlet weak var btnVersion: UIButton!

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            self.selectionStyle = .none
        }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension MentorProfile : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtOldPassword   || textField == txtNewPassword || textField == txtConfirmPassword {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 50)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
