//
//  MyExpertiseVC.swift
//  YuvaShastra
//
//  Created by Navin Patidar on 4/2/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit

class MyExpertiseVC: UIViewController {
    
    @IBOutlet weak var tvlist: UITableView!
    var aryList = NSMutableArray()
    
    // MARK: - ----------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "My Expertise"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        aryList = NSMutableArray()
        aryList = (dict_Login_Data.value(forKey: "userCategoryMappingDc")as! NSArray).mutableCopy()as! NSMutableArray
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tvlist.dataSource = self
        tvlist.delegate = self
        tvlist.reloadData()
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MyExpertiseVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ExpertiseCell", for: indexPath as IndexPath) as! ExpertiseCell
        let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        cell.lblTitle.text = "\(dict.value(forKey: "CategoryName")!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    
    
    
}

// MARK: - ----------------ChatCell
// MARK: -
class ExpertiseCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
