//
//TAG = 1  Qualification        Registration Screen
//TAG = 21 ,22           Registration Screen (Aspirent)

//TAG = 31 Mentor List       Mentor DashBoard
//TAG = 41 , 42, 43 Add Question List

import UIKit

//MARK: ---------------Protocol-----------------

protocol PopUpDelegate : class{
    func getDataFromPopupDelegate(dictData : NSDictionary ,tag : Int)
}


class PopUpView: UIViewController {
    
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var height_ForTBLVIEW: NSLayoutConstraint!
    @IBOutlet weak var viewFortv: CardView!

    
    var strTitle = String()
    var aryTBL = NSMutableArray()
    var strTag = Int()
    
    
    open weak var delegate:PopUpDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        tvForList.estimatedRowHeight = 50.0
        btnTitle.setTitle(strTitle, for: .normal)
        tvForList.tableFooterView = UIView()
        tvForList.dataSource = self
        tvForList.delegate = self
        self.view.backgroundColor = UIColor.clear
        print(strTag)
        btnTitle.backgroundColor = hexStringToUIColor(hex: primaryBlueColor)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
     
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        height_ForTBLVIEW.constant  = 0.0
        height_ForTBLVIEW.constant = CGFloat((aryTBL.count * 42 ) + 45 )
        if  height_ForTBLVIEW.constant > self.view.frame.size.height - 150  {
            height_ForTBLVIEW.constant = self.view.frame.size.height - 150
        }
        self.viewFortv.frame = CGRect(x: 15, y: Int(self.view.frame.height) / 2 - Int(self.height_ForTBLVIEW.constant) / 2 , width: Int(self.view.frame.width) - 30 , height: Int(self.height_ForTBLVIEW.constant))
        self.viewFortv.center = self.view.center
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
      self.dismiss(animated: false) { }
        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PopUpView : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTBL.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvForList.dequeueReusableCell(withIdentifier: "popupCell", for: indexPath as IndexPath) as! popupCell
        let dict = aryTBL.object(at: indexPath.row)as! NSDictionary
        //Ragistration Screen
        if(strTag == 1 || strTag == 21)
        {
            cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Qualification")!)"
        }else if(strTag == 21 || strTag == 31 || strTag == 41)
        {
            cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "CategoryName")!)"
        }  else if(strTag == 42)
        {
            cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "SubCategoryName")!)"
        }else if(strTag == 43)
        {
            cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "NestedSubCategoryName")!)"
        }else if(strTag == 22)
        {
            cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "name")!)"
        }
        
      
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryTBL.object(at: indexPath.row)as? NSDictionary
        self.delegate?.getDataFromPopupDelegate(dictData: dict!, tag: self.strTag)
        self.dismiss(animated: false) {}
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
}

class popupCell: UITableViewCell {
    @IBOutlet weak var popupCell_lbl_Title: UILabel!
}


