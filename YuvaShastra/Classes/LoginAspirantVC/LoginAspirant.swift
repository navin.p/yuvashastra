//
//  LoginAspirant.swift
//  YuvaShastra
//
//  Created by Rakesh Jain on 11/03/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit



class LoginAspirant: UIViewController {
   
    //MARK: IBOutlet
    @IBOutlet weak var constLblLine_L: NSLayoutConstraint!

    @IBOutlet weak var lblindicate: UILabel!
    @IBOutlet weak var btnLoginTopBar: UIButton!
    @IBOutlet weak var btnRegisterTopBar: UIButton!
    @IBOutlet weak var scroll: UIScrollView!
    
    @IBOutlet var loginView: CardView!
    @IBOutlet weak var txtLoginMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var txtLoginPassword: ACFloatingTextfield!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnKeepMeLogin: UIButton!

    @IBOutlet var registerView: CardView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtRegister_FullName: ACFloatingTextfield!
    @IBOutlet weak var txtRegister_DOB: ACFloatingTextfield!
    @IBOutlet weak var txtRegister_MobileNumber: ACFloatingTextfield!
    @IBOutlet weak var txtRegister_AdditionalQualification: ACFloatingTextfield!
    @IBOutlet weak var heightTblQualification: NSLayoutConstraint!
    @IBOutlet weak var tvlist: UITableView!

    @IBOutlet weak var txtClass: ACFloatingTextfield!
    @IBOutlet weak var txtPassedIn: ACFloatingTextfield!
    @IBOutlet weak var txtScore: ACFloatingTextfield!
    
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var picker: UIDatePicker!
    
    var aryList = NSMutableArray()
    var aryQualification = NSMutableArray()

    // MARK:
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavBar(strtilte: "")
        tvlist.tableFooterView = UIView()
         heightTblQualification.constant = 0.0
        btnLogin.layer.borderColor = UIColor.darkGray.cgColor
        btnLogin.layer.borderWidth = 1.0
        btnNext.layer.borderColor = UIColor.darkGray.cgColor
        btnNext.layer.borderWidth = 1.0
        constLblLine_L.constant = 0
        loginView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: loginView.frame.height)
        scroll.addSubview(loginView)
        scroll.contentSize = CGSize (width: scroll.frame.width, height: loginView.frame.height)
     
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(self.view.tag == 99){
           self.addLoginView()
        }
          call_GetQualificationDropdown_API()
    }
    func setUpNavBar(strtilte : String){
        //    self.navigationItem.title = strtilte
        let logo = UIImage(named: "toolbar_logo")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
    }
    // MARK:
    // MARK: - Actions
    @IBAction func actionOnBack(_ sender: UIButton) {
             self.navigationController?.popViewController(animated: true)
    }
  
    
    @IBAction func actionOnLogin_Register(_ sender: UIButton) {
        if(sender.tag == 1){
            self.view.endEditing(true)
            btnLoginTopBar.setTitleColor(hexStringToUIColor(hex: primaryBlueColor), for: .normal)
            btnRegisterTopBar.setTitleColor(UIColor.darkGray, for: .normal)
            constLblLine_L.constant = 0
            UIView.animate(withDuration: 0.3)
            {
                self.view.layoutIfNeeded()
            }
            registerView.removeFromSuperview()
            loginView.shadowColor = UIColor.clear
            loginView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: loginView.frame.height)
            scroll.addSubview(loginView)
            scroll.contentSize = CGSize (width: scroll.frame.width, height: loginView.frame.height)
        }else{
            self.view.endEditing(true)
            btnRegisterTopBar.setTitleColor(hexStringToUIColor(hex: primaryBlueColor), for: .normal)
            btnLoginTopBar.setTitleColor(UIColor.darkGray, for: .normal)
            constLblLine_L.constant = btnLoginTopBar.frame.width
            UIView.animate(withDuration: 0.3)
            {
                self.view.layoutIfNeeded()
            }
            loginView.removeFromSuperview()
            registerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: registerView.frame.height)
            registerView.shadowColor = UIColor.clear
            scroll.addSubview(registerView)
            scroll.contentSize = CGSize (width: scroll.frame.width, height: registerView.frame.height + heightTblQualification.constant)
      }
    }
    //MARK:
    //MARK: Login View
    
    @IBAction func actionOnPasswordShow(_ sender: UIButton) {
        sender.currentBackgroundImage == (UIImage(named: "hidePassword")) ? (txtLoginPassword.isSecureTextEntry = false) : (txtLoginPassword.isSecureTextEntry = true)
        sender.currentBackgroundImage == (UIImage(named: "hidePassword")) ? sender.setBackgroundImage(UIImage(named: "showPassword"), for: .normal) : sender.setBackgroundImage(UIImage(named: "hidePassword"), for: .normal)
        
    }
    
    @IBAction func actionOnKeepmelogin(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.currentImage == (UIImage(named: "uncheck")) ? sender.setImage(UIImage(named: "check"), for: .normal) : sender.setImage(UIImage(named: "uncheck"), for: .normal)
    }
    
    @IBAction func actionOnForgotPassword(_ sender: Any) {
        self.view.endEditing(true)
        viewForgotAlert()
    }
    
    @IBAction func actionOnLoginButton(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if txtLoginMobileNumber.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_MobileNumber, viewcontrol: self)
        }else if (txtLoginMobileNumber.text?.count)! < 10
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_MobileNumberLimit, viewcontrol: self)
        }
        else if txtLoginPassword.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_Password, viewcontrol: self)
        }
        else{
            self.LoginApiCalling(sender: sender)
        }
    }
    
    //MARK:
    //MARK: RegisterView

    @IBAction func actionOnDoB(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewPicker.frame = self.view.frame
        picker.maximumDate = Date()
        self.view.addSubview(self.viewPicker)
    }
    

    
    @IBAction func actionOnNext(_ sender: Any) {
        if txtRegister_FullName.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Full name required", viewcontrol: self)
        }
        else if txtRegister_DOB.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "DOB required", viewcontrol: self)
        }
     
        else if txtRegister_MobileNumber.text?.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_MobileNumber, viewcontrol: self)
        }
        else if (txtRegister_MobileNumber.text?.count)! < 10
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_MobileNumberLimit, viewcontrol: self)
        }  else if aryList.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Qualification required", viewcontrol: self)
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "SetMentorProfileVC")as! SetMentorProfileVC
            let dictData = NSMutableDictionary()
            dictData.setValue("0", forKey: "UserId")
            dictData.setValue("1", forKey: "city_id")
            dictData.setValue(txtRegister_FullName.text!, forKey: "Name")
            dictData.setValue(txtRegister_MobileNumber.text!, forKey: "MobileNumber")
            dictData.setValue("", forKey: "EmailId")
            dictData.setValue(txtRegister_MobileNumber.text!, forKey: "UserName")
            dictData.setValue("", forKey: "password")
            dictData.setValue(txtRegister_DOB.text!, forKey: "DOB")
            dictData.setValue("Student", forKey: "Role")
            
            dictData.setValue("YuvaShastra", forKey: "SocialMediaType")
            dictData.setValue("", forKey: "CurruntPosition")
            dictData.setValue("", forKey: "AwardRecognition")
            dictData.setValue("", forKey: "ProfileImage")
            dictData.setValue("true", forKey: "IsApproveMentor")
            dictData.setValue("true", forKey: "IsActive")
            dictData.setValue("false", forKey: "IsDeleted")
            dictData.setValue("", forKey: "CreatedDate")
            dictData.setValue("", forKey: "userCategoryMappingDc")
            dictData.setValue(aryList, forKey: "userQualificationMappingDc")
            testController.dictRegisterData = dictData
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
    }
    
    @IBAction func actionOnAddQualification(_ sender: Any) {
        self.view.endEditing(true)
        if (txtClass.text!.count == 0){
        }else if (txtPassedIn.text!.count == 0){
        }else if (txtScore.text!.count == 0){
        }else if (aryList.count > 4){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_QualificationLimit, viewcontrol: self)
        }
        
        else{
            let dict = NSMutableDictionary()
            
            dict.setValue("0", forKey: "UserQualificationId")
            dict.setValue("0", forKey: "UserId")
            dict.setValue(txtPassedIn.text!, forKey: "PassingYear")
            dict.setValue("1", forKey: "PassingDivision")
            dict.setValue(txtScore.text!, forKey: "ScorePer")
            dict.setValue("true", forKey: "IsActive")
            dict.setValue("false", forKey: "IsDeleted")
            dict.setValue(txtClass.text!, forKey: "Qualification")
            dict.setValue("\(txtClass.tag)", forKey: "QualificationId")
            aryList.add(dict)
            tvlist.reloadData()
            heightTblQualification.constant = CGFloat((aryList.count * 60))
            scroll.contentSize = CGSize (width: scroll.frame.width, height: registerView.frame.height + heightTblQualification.constant)
            txtScore.text = ""
            txtPassedIn.text = ""
            txtClass.text = ""
            txtScore.tag = 0
            txtPassedIn.tag = 0
            txtClass.tag = 0
        }
    }
    
    @IBAction func actionOnClassDropDown(_ sender: Any) {
        if(aryQualification.count != 0){
             OpenPopListView(strTag: 21, aryTemp: aryQualification)
        }else{
             self.call_GetQualificationDropdown_API()
        }
    }
    
    @IBAction func actionOnPassedInDropDown(_ sender: Any) {
        self.view.endEditing(true)
        let aryYear = NSMutableArray()
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy"
        let formattedDate = format.string(from: date)
        print(formattedDate)
        let list = [Int](1960...Int("\(formattedDate)")!)
        for (index, element) in list.enumerated() {
            let dict = NSMutableDictionary()
            dict.setValue("\(element)", forKey: "name")
            dict.setValue("\(index)", forKey: "id")
            aryYear.add(dict)
        }
        OpenPopListView(strTag: 22, aryTemp: (aryYear.reversed()as NSArray).mutableCopy() as! NSMutableArray)
    }
    
    
    //MARK:
    //MARK: PickerView
    
    @IBAction func actionOnPickerDone_Cancel(_ sender: UIButton) {
        if(sender.tag == 1){
                self.viewPicker.removeFromSuperview()
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy"
            let somedateString = dateFormatter.string(from: picker.date)
            txtRegister_DOB.text = somedateString
            self.viewPicker.removeFromSuperview()
        }
     
    }
    // MARK:
    // MARK: - Extra Function
 
    func addLoginView()  {
        constLblLine_L.constant = 0
        loginView.shadowColor = UIColor.clear
        loginView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: loginView.frame.height)
        scroll.addSubview(loginView)
        scroll.contentSize = CGSize (width: scroll.frame.width, height: loginView.frame.height)
        call_GetQualificationDropdown_API()
        txtLoginMobileNumber.text = ""
        txtLoginPassword.text = ""
    }
    func viewForgotAlert()  {
        let alertController = UIAlertController(title: "Forgot Password", message: "Enter the registrered mobile number associated with your  account in the text box and then click the Submit  button.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Submit", style: .default, handler: { alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            textField.keyboardType = .numberPad
            if !(validateEmail(email: textField.text!)){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: login_MobileNumberLimit, viewcontrol: self)
                self.viewForgotAlert()
            }else{
                self.callForgotPasswordAPI(sender: textField.text!)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Mobile Number"
            textField.borderStyle = UITextField.BorderStyle.roundedRect
            textField.delegate = self
            textField.tag = 99
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    func OpenPopListView(strTag : Int , aryTemp : NSMutableArray) {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "---Select---"
        vc.strTag = strTag
        if aryTemp.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.aryTBL = aryTemp
            vc.delegate = self
            self.present(vc, animated: false, completion: {})
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "", viewcontrol: self)
        }
        
    }
    // MARK:
    // MARK: - API Calling
    func LoginApiCalling(sender : UIButton){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            
            let urlForGetData = "\(API_GetLogin)" + "username=\(self.txtLoginMobileNumber.text!)" + "&password=\(self.txtLoginPassword.text!)"
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: urlForGetData) { (responce, status) in
                FTIndicator.dismissProgress()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.btnKeepMeLogin.currentImage == UIImage(named: "check") ? nsud.setValue("true", forKey: "Yuva_LoginRemberStatus") :  nsud.setValue("false", forKey: "Yuva_LoginRemberStatus")
                        nsud.setValue(removeNullFromDict(dict: (dict.value(forKey: "RegistrationDc")as! NSDictionary).mutableCopy()as! NSMutableDictionary), forKey: "Yuva_LoginData")
                        deleteAllRecords(strEntity:"LoginData")
                        let aryTemp = NSMutableArray()
                        aryTemp.add(removeNullFromDict(dict: (dict.value(forKey: "RegistrationDc")as! NSDictionary).mutableCopy()as! NSMutableDictionary))
                        saveDataInLocalArray(strEntity: "LoginData", strKey: "loginData", data: aryTemp)
                        
                        if ("\(((dict.value(forKey: "RegistrationDc")as! NSDictionary).value(forKey: "IsVerify")!))" == "1"){
                            self.navigationController?.setNavigationBarHidden(true, animated: true)
                            
                            let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }else{
                            let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVarifyVC") as! OTPVarifyVC
                            nextViewController.strViewComeFrom = "Login"
                            let dict = NSMutableDictionary()
                            dict.setValue(self.txtLoginMobileNumber.text!, forKey: "UserName")
                            nextViewController.dictData = dict
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_GetQualificationDropdown_API( )  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...")
            
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: API_GetQualificationDropdown) { (responce, status) in
                FTIndicator.dismissProgress()
                
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let aryData  = (dict.value(forKey: "qualificationDropdownDc")as! NSArray)
                        self.aryQualification = NSMutableArray()
                        self.aryQualification = aryData.mutableCopy()as! NSMutableArray
                     //   self.OpenPopListView(strTag: 21, aryTemp:  self.aryQualification)
                    }else{
                    }
                }else{
                }
            }
        }
    }
    func callForgotPasswordAPI(sender : String) {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...")
            WebService.callAPIBYGET(parameter: NSMutableDictionary(), url: API_GetQualificationDropdown) { (responce, status) in
                FTIndicator.dismissProgress()
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ----------------UITextFieldDelegate
// MARK: -
extension LoginAspirant : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtLoginMobileNumber || textField == txtRegister_MobileNumber )
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        else if (  textField == txtLoginPassword )
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)
        }
        else if (  textField == txtRegister_FullName ||  textField == txtRegister_AdditionalQualification )
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)
        }
        else if (  textField == txtScore )
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 2)
        }
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension LoginAspirant: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "LoginAspirantCell", for: indexPath as IndexPath) as! LoginAspirantCell
        let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        cell.lblScore.text = "Score:\n\(dict.value(forKey: "ScorePer")!)"
        cell.lblPassedIn.text = "Passed In:\n\(dict.value(forKey: "PassingYear")!)"
        cell.lblCource.text = "Class:\n\(dict.value(forKey: "Qualification")!)"
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
   
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            aryList.removeObject(at: indexPath.row)
            tvlist.reloadData()
              heightTblQualification.constant = 0.0
            heightTblQualification.constant = CGFloat((aryList.count * 50))
            scroll.contentSize = CGSize (width: scroll.frame.width, height: registerView.frame.height + heightTblQualification.constant)
        }
    }
    
    
}
// MARK: - ----------------ChatCell
// MARK: -
class LoginAspirantCell: UITableViewCell {
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblPassedIn: UILabel!
    @IBOutlet weak var lblCource: UILabel!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension LoginAspirant : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 21){
       
            self.txtClass.text = "\(dictData.value(forKey: "Qualification")!)"
            self.txtClass.tag = Int("\(dictData.value(forKey: "QualificationId")!)")!
    
        }else if(tag == 22){
            self.txtPassedIn.text = "\(dictData.value(forKey: "name")!)"
            self.txtPassedIn.tag = Int("\(dictData.value(forKey: "id")!)")!
        }
    }
}
