//
//  HomeVC.swift
//  YuvaShastra
//
//  Created by Rakesh Jain on 11/03/19.
//  Copyright © 2019 Infocrats. All rights reserved.
//

import UIKit
import CoreGraphics

class HomeVC: UIViewController
{

// MARK:
// MARK: - Global Variables
    var arySlider = NSMutableArray()
    
// MARK: -
// MARK: - Outlets

    @IBOutlet weak var viewPageSlider: CPImageSlider!
    @IBOutlet weak var btnMentor: UIButton!
    @IBOutlet weak var btnAspirant: UIButton!

    // MARK: -
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        arySlider = [["title":"About Mandir","image":"Home"],["title":"How To Reach","image":"Home"],["title":"Live Darshan","image":"Home"]]
    }
    
    override func viewWillAppear(_ animated: Bool) {
           self.setSliderImages(aryimageData: arySlider)
        super.viewWillAppear(true)
        btnMentor.layer.cornerRadius = 8.0
        btnAspirant.layer.cornerRadius = 8.0
        let logo = UIImage(named: "toolbar_logo")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        let newBackButton = UIBarButtonItem()
        newBackButton.title = "Back"
        navigationController?.navigationBar.topItem?.backBarButtonItem = newBackButton
    }
 
    func setSliderImages(aryimageData : NSMutableArray) {
        viewPageSlider.images =  arySlider
        viewPageSlider.delegate = self
        viewPageSlider.autoSrcollEnabled = true
        viewPageSlider.enableArrowIndicator = false
        viewPageSlider.enablePageIndicator = true
        viewPageSlider.enableSwipe = true
    }


    // MARK:
    // MARK: - Actions
  
   
    @IBAction func actionOnMentor_Aspirants(_ sender: UIButton) {
              viewPageSlider.stopAutoPlay()
        if(sender.tag == 1){
            let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginMentor") as! LoginMentor
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }else{
            let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginAspirant") as! LoginAspirant
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
   
}
// MARK: - ----------------CPSliderDelegate
// MARK: -
extension  HomeVC  : CPSliderDelegate{
    func sliderImageIndex(slider: CPImageSlider, index: Int) {
    }
    
    func sliderImageTapped(slider: CPImageSlider, index: Int) {
    }
}
