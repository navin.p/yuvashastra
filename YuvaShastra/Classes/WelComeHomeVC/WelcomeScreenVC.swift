//
//  WelcomeScreenVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/11/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class WelcomeScreenVC: UIViewController {

    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnDone: UIButton!

    @IBOutlet weak var pageIndicator: UIPageControl!
    @IBOutlet weak var collectionForWelcome: UICollectionView!
    
    //MARK:
    //MARK: CustomeVariable

    let  ary_CollectionData = [
        ["title":"Welcome - 1 ",
         "subtitle" :" है कौन विघ्न ऐसा जग में, टिक सके आदमी के मग में? ख़म ठोंक ठेलता है जब नर पर्वत के जाते पाव उखड़, मानव जब जोर लगता है, पत्थर पानी बन जाता है । गुन बड़े एक से एक प्रखर, है छिपे मानवों के भितर, मेहंदी में जैसी लाली हो, वर्तिका - बीच उजियाली हो, बत्ती जो नहीं जलाता है, रोशनी नहीं वह पता है । \n\n रामधारी सिंह दिनकर  ",
         "image":"info_1"],
        
        ["title":"Welcome  - 2",
         "subtitle" :"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ",
         "image":"info_2"],
        ["title":"Welcome - 3",
         "subtitle" :"sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque.",
         "image":"info_3"]]

    
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
 

        self.btnNext.layer.borderWidth = 1.0
        self.btnNext.layer.borderColor = UIColor.lightGray.cgColor
        self.btnSkip.layer.borderWidth = 1.0
        self.btnSkip.layer.borderColor = UIColor.lightGray.cgColor
        self.btnDone.layer.borderWidth = 1.0
        self.btnDone.layer.borderColor = UIColor.lightGray.cgColor
        self.btnDone.isHidden = true
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnSkip(_ sender: UIButton) {
        sender.backgroundColor = hexStringToUIColor(hex: primaryColor)
        sender.setTitleColor(UIColor.white, for: .normal)

        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(UIColor.black, for: .normal)

            let testController = mainStoryboard.instantiateViewController(withIdentifier: "MentorDashboardVC")as! MentorDashboardVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    @IBAction func actionOnDone(_ sender: UIButton) {
        sender.backgroundColor = hexStringToUIColor(hex: primaryColor)
        sender.setTitleColor(UIColor.white, for: .normal)
        sender.alpha = 0.4
        btnNext.isHidden = true
        btnSkip.isHidden = true
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(UIColor.black, for: .normal)
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "MentorDashboardVC")as! MentorDashboardVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    @IBAction func actionOnNext(_ sender: UIButton) {
        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
          sender.backgroundColor = hexStringToUIColor(hex: primaryColor)
        sender.setTitleColor(UIColor.white, for: .normal)

        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 1
            sender.transform = .identity

        }) { (result) in
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(UIColor.black, for: .normal)

            if self.pageIndicator.currentPage == 0 {
                self.btnDone.isHidden = true
               self.collectionForWelcome?.scrollToItem(at:IndexPath(item: 1, section: 0), at: .right, animated: true)
                self.pageIndicator.currentPage = (1)
                
            }else if (self.pageIndicator.currentPage == 1 ){
                self.collectionForWelcome?.scrollToItem(at:IndexPath(item: 2, section: 0), at: .right, animated: true)
                self.pageIndicator.currentPage = (2)
                    self.btnDone.isHidden = false
                
            }else if (self.pageIndicator.currentPage == 2 ){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "MentorDashboardVC")as! MentorDashboardVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
    }
}

// MARK: - --------------UICollectionView
// MARK:
extension WelcomeScreenVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ary_CollectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "welcomeCell", for: indexPath as IndexPath) as! welcomeCell
        let dict = ary_CollectionData[indexPath.row]as NSDictionary
        cell.welcome_lbl_Title.text = dict["title"]as? String
        cell.welcome_lbl_SubTitle.text = dict["subtitle"]as? String
        cell.welcome_Image.image = UIImage(named: dict["image"]as! String)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width) , height:(self.collectionForWelcome.frame.size.height))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
       cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let x = self.collectionForWelcome.contentOffset.x
        let w = self.collectionForWelcome.bounds.size.width
        let currentPage = Int(ceil(x/w))
        pageIndicator.currentPage = (currentPage)
        if currentPage == 2 {
            self.btnDone.isHidden = false
            self.btnNext.isHidden = true
            self.btnSkip.isHidden = true

        }else{
            self.btnDone.isHidden = true
            self.btnNext.isHidden = false
            self.btnSkip.isHidden = false
        }
    }
}
class welcomeCell: UICollectionViewCell {
    //Welcome Screen
    @IBOutlet weak var welcome_Image: UIImageView!
    @IBOutlet weak var welcome_lbl_Title: UILabel!
    @IBOutlet weak var welcome_lbl_SubTitle: UILabel!
}
